import React from "react";
import Script from "next/script";

const JsPlugins = () => {
    return (
        <>
            <Script
                id="jquery"
                src="/js/jquery-3.3.1.min.js"
                className="jquery"
                strategy="beforeInteractive"
            />
            <Script
                id="bootstrap"
                src="/js/bootstrap.min.js"
                className="bootstrap"
                strategy="beforeInteractive"
            />
            <Script id="niceSelect" src="/js/jquery.nice-select.min.js" className="niceSelect" />
            <Script id="jqueryUi" src="/js/jquery-ui.min.js" className="jqueryUi" />
            <Script id="slicknav" src="/js/jquery.slicknav.js" className="slicknav" />
            <Script id="mixitup" src="/js/mixitup.min.js" className="mixitup" />
            <Script
                id="owlCarousel"
                src="/js/owl.carousel.min.js"
                className="owlCarousel"
                strategy="afterInteractive"
            />
            <Script src="/js/toastr.min.js" strategy="beforeInteractive" />
            <Script id="main" src="/js/main.js" className="main" strategy="lazyOnload" />
        </>
    );
};

export default JsPlugins;

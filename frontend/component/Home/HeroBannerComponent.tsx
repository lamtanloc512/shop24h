import React, { FC } from "react";
import Image from "next/image";
import styled from "styled-components";

const HeroBannerComponent: FC = () => {
    return (
        <div className="hero__item position-relative">
            <div className="hero__wrapper">
                <Image
                    className="hero__banner"
                    src="/img/hero/banner.jpg"
                    layout="fill"
                    objectFit="cover"
                    loading="lazy"
                />
            </div>

            <div className="hero__text position-absolute px-2 py-3">
                <span>AMAZING CAMERA</span>
                <h3 className="fw-bold">
                    Best Camera <br />
                    100% Guarante
                </h3>
                <p className="mb-2">Free Pickup and Delivery Available</p>
                <a className="btn btn-primary fw-bold">SHOP NOW</a>
            </div>
        </div>
    );
};

export default HeroBannerComponent;

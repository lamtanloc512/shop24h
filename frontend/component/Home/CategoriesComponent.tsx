import React, { FC, useEffect } from "react";
import $ from "jquery";

const CategoriesComponent: FC = () => {
    useEffect(() => {
        $(".hero__categories__all").on("click", function () {
            $(".hero__categories ul").slideToggle(300);
        });
    }, []);

    return (
        <div className="hero__categories">
            <div className="hero__categories__all">
                <i className="fa fa-bars" />
                <span>Categories</span>
            </div>
            <ul>
                <li>
                    <a href="#">Apple iPhone</a>
                </li>
                <li>
                    <a href="#">Apple Macbook</a>
                </li>
                <li>
                    <a href="#">Apple iPad</a>
                </li>
                <li>
                    <a href="#">Apple SmartWatch</a>
                </li>
                <li>
                    <a href="#">Sony Camera</a>
                </li>
                <li>
                    <a href="#">JBL Headphone</a>
                </li>
                <li>
                    <a href="#">JBL Speaker</a>
                </li>
                <li>
                    <a href="#">Xbox One</a>
                </li>
                <li>
                    <a href="#"> B &amp; O Headphone</a>
                </li>
                <li>
                    <a href="#">Monitor</a>
                </li>
                <li>
                    <a href="#">Oatmeal</a>
                </li>
            </ul>
        </div>
    );
};

export default CategoriesComponent;

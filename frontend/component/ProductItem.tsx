import React, { FC, useContext, useEffect } from "react";
import Link from "next/link";
import Image from "next/image";
import { CartContext } from "./../context/CartContextProvider";
import { CartItem } from "../reducer/ShoppingCartReducer";
import { ProductData } from "../utils/interface";

interface IDataFeature {
    payload: ProductData;
}

const ProductItem: FC<IDataFeature> = ({ payload }) => {
    const { addToCartAction } = useContext(CartContext);
    //add to cart when click icon cart
    const addToCart = (): void => {
        const vPayload: CartItem = {
            id: payload.id,
            name: payload.productName,
            price: payload.price,
            qty: 1,
            imgUrl: payload.productMainImg.imgPermalink,
        };
        addToCartAction(vPayload);
    };
    return (
        <div className="featured__item">
            <div className="featured__item__pic">
                <Link
                    href={{
                        pathname: `/shop/products/${payload.id}`,
                        query: { id: payload.id },
                    }}
                >
                    <a className={`product-${payload.id}`}>
                        <Image
                            src={payload.productMainImg.imgPermalink}
                            layout="fill"
                            objectFit="contain"
                            alt="item__pic"
                            quality={100}
                        />
                    </a>
                </Link>
                <ul className="featured__item__pic__hover">
                    <li>
                        <a href="#">
                            <i className="fa fa-heart" />
                        </a>
                    </li>
                    {/* <li>
                        <a href="#">
                            <i className="fa fa-retweet" />
                        </a>
                    </li> */}
                    <li>
                        <a
                            className="a__shopping__cart"
                            onClick={addToCart}
                            style={{ cursor: "pointer" }}
                        >
                            <i className="fa fa-shopping-cart" />
                        </a>
                    </li>
                </ul>
            </div>
            <div className="featured__item__text">
                <h6>
                    <Link
                        href={{
                            pathname: `/shop/products/${payload.id}`,
                            query: { id: payload.id },
                        }}
                    >
                        <a className={`product-${payload.id} fs-5`} data-id={payload.id}>
                            {payload.productName}
                        </a>
                    </Link>
                </h6>
                <h5>${payload.price}</h5>
            </div>
        </div>
    );
};

export default ProductItem;

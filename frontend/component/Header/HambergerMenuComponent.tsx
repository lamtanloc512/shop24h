import React, { FC } from "react";
import CartItemComponent from "./CartItemComponent";
import Link from "next/link";
import Routes from "../RoutePaths/Routes";
import { useRouter } from "next/router";

const HambergerMenuComponent: FC = () => {
    const router = useRouter();
    return (
        <>
            <div className="humberger__menu__overlay" />
            <div className="humberger__menu__wrapper">
                <div className="humberger__menu__logo">
                    <a href="#">
                        <h2>DEVCAMP</h2>
                    </a>
                </div>
                <div className="humberger__menu__widget">
                    <div className="header__top__right__language">
                        <img src="/img/language.png" alt="" />
                        <div>English</div>
                        <span className="arrow_carrot-down" />
                        <ul>
                            <li>
                                <Link href={"/"}>
                                    <a>Vietnamese</a>
                                </Link>
                            </li>
                            <li>
                                <Link href={"/"}>
                                    <a>English</a>
                                </Link>
                            </li>
                        </ul>
                    </div>
                    <div className="header__top__right__auth ms-4">
                        <Link href={"/"}>
                            <a>
                                <i className="fa fa-user" /> Login
                            </a>
                        </Link>
                    </div>
                </div>
                <div className="humberger__menu__cart">
                    <ul>
                        <CartItemComponent />
                    </ul>
                </div>

                <nav className="humberger__menu__nav mobile-menu">
                    <ul>
                        {Routes.map((bI, index) => (
                            <li
                                key={index}
                                className={
                                    router.asPath == bI.path
                                        ? "header__menu__item active"
                                        : "header__menu__item"
                                }
                            >
                                <Link href={bI.path}>{bI.label}</Link>
                            </li>
                        ))}
                    </ul>
                </nav>
                <div id="mobile-menu-wrap" />
                <div className="header__top__right__social">
                    <a href="#">
                        <i className="fa fa-facebook" />
                    </a>
                    <a href="#">
                        <i className="fa fa-twitter" />
                    </a>
                    <a href="#">
                        <i className="fa fa-linkedin" />
                    </a>
                    <a href="#">
                        <i className="fa fa-pinterest-p" />
                    </a>
                </div>
                <div className="humberger__menu__contact">
                    <ul>
                        <li>
                            <i className="fa fa-envelope" /> loclt@devcamp.edu.vn
                        </li>
                    </ul>
                </div>
            </div>
        </>
    );
};

export default HambergerMenuComponent;

import React, { FC, useState, useEffect, useContext } from "react";
import { Offcanvas, Button } from "react-bootstrap";
import { CartContext } from "../../context/CartContextProvider";
import Image from "next/image";
import { CartItem } from "../../reducer/ShoppingCartReducer";

const OffCanvasCartComponent: FC<any> = ({ show, handleclose, placement, ...props }) => {
    const { cartList, increaseQtyCart, decreaseQtyCart, deleteProductCart, changeQtyProduct } =
        useContext(CartContext);
    const [totalPrice, setTotalPrice] = useState<number>(0);

    useEffect(() => {
        let total = 0;
        for (let index = 0; index < cartList.length; index++) {
            total = total + cartList[index].qty * parseInt(cartList[index].price);
        }
        setTotalPrice(total);
    }, [cartList]);

    const handleChangeQty = (e: React.ChangeEvent<HTMLInputElement>) => {
        const dataChangeElementJson = e.currentTarget.dataset.element;
        let payload = {};
        if (dataChangeElementJson) {
            payload = {
                ...JSON.parse(dataChangeElementJson),
                qty: e.target.value,
            };
        }
        changeQtyProduct(payload);
    };

    const increaseQty = (e: React.SyntheticEvent<HTMLButtonElement>) => {
        // console.log(e.currentTarget.dataset.element);
        const dataElementJson = e.currentTarget.dataset.element;
        let payload = {};
        if (dataElementJson) {
            payload = JSON.parse(dataElementJson);
        }
        // console.log(payload);
        increaseQtyCart(payload);
    };

    const decreaseQty = (e: React.SyntheticEvent<HTMLButtonElement>) => {
        // console.log(e.currentTarget.dataset.element);
        const dataElementJson = e.currentTarget.dataset.element;
        let payload = {};
        if (dataElementJson) {
            payload = JSON.parse(dataElementJson);
        }
        // console.log(payload);
        decreaseQtyCart(payload);
    };

    const handleDelete = (e: React.SyntheticEvent<HTMLElement>) => {
        // console.log(e.currentTarget.dataset.element);
        const dataDeleteElementJson = e.currentTarget.dataset.element;
        let payload = {};
        if (dataDeleteElementJson) {
            payload = JSON.parse(dataDeleteElementJson);
        }
        deleteProductCart(payload);
    };

    return (
        <>
            <Offcanvas show={show} onHide={handleclose} {...props} placement={placement}>
                <Offcanvas.Header closeButton>
                    <Offcanvas.Title>
                        <h4 className="fw-bold">Shopping Cart</h4>
                    </Offcanvas.Title>
                </Offcanvas.Header>
                <Offcanvas.Body>
                    {cartList.map((cartItem: CartItem, index: number) => {
                        return (
                            <div className="card mx-2 my-4" key={index}>
                                <div className="card-body">
                                    <div className="row">
                                        <div className="col-4 d-flex align-items-center">
                                            {cartItem.imgUrl ? (
                                                <Image
                                                    src={cartItem.imgUrl}
                                                    height={200}
                                                    width={200}
                                                />
                                            ) : (
                                                <Image
                                                    src="/img/dummy.jpg"
                                                    height={200}
                                                    width={200}
                                                />
                                            )}
                                        </div>
                                        <div className="col">
                                            <div className="card-text fw-bold">{cartItem.name}</div>
                                            <div className="card-text d-flex">
                                                <h5 className="me-2">Price: </h5>
                                                <strong>{`${
                                                    cartItem.qty * parseInt(cartItem.price)
                                                }$`}</strong>
                                            </div>
                                            <div className="form-group my-2 d-flex">
                                                <button
                                                    className="btn btn-sm btn-danger mx-1"
                                                    onClick={decreaseQty}
                                                    data-element={JSON.stringify(cartItem)}
                                                >
                                                    -
                                                </button>
                                                <input
                                                    className="form-control form-control-sm mx-1"
                                                    type="number"
                                                    value={cartItem.qty}
                                                    onChange={handleChangeQty}
                                                    data-element={JSON.stringify(cartItem)}
                                                />
                                                <button
                                                    className="btn btn-sm btn-primary mx-1"
                                                    onClick={increaseQty}
                                                    data-element={JSON.stringify(cartItem)}
                                                >
                                                    +
                                                </button>
                                            </div>
                                        </div>

                                        <div className="col-1 ps-0 pe-2 d-flex justify-content-end align-items-start">
                                            <i
                                                className="fa fa-times-circle"
                                                aria-hidden="true"
                                                style={{ cursor: "pointer" }}
                                                onClick={handleDelete}
                                                data-element={JSON.stringify(cartItem)}
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        );
                    })}
                    <div className="card mx-2">
                        <div className="card-body">
                            <div className="card-text">
                                <span className="d-flex">
                                    <h4 className="fw-bold mx-2">Total Price: </h4>
                                    <h4>{totalPrice}$</h4>
                                </span>
                            </div>
                        </div>
                    </div>
                </Offcanvas.Body>
            </Offcanvas>
        </>
    );
};

export default OffCanvasCartComponent;

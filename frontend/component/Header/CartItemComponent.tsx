import React, { FC, useState, useContext, useEffect } from "react";
import Link from "next/link";
import OffCanvasCartComponent from "./OffCanvasCartComponent";
import { CartContext } from "../../context/CartContextProvider";

const CartItemComponent: FC = () => {
    const { cartList } = useContext(CartContext);
    const [itemNumbers, setItemNumbers] = useState<number>(0);
    const [show, setShow] = useState<boolean>(false);
    const handleClose = (): void => setShow(false);
    const handleShow = (): void => setShow(true);

    useEffect(() => setItemNumbers(cartList.length), [cartList]);

    return (
        <>
            <div className="header__cart">
                <ul>
                    {/* <li>
                        <a href="#">
                            <i className="fa fa-heart" /> <span>1</span>
                        </a>
                    </li> */}
                    <li>
                        <a type="button" onClick={handleShow}>
                            <i
                                className="fa fa-shopping-bag header__cart__icon"
                                style={{ fontSize: "20px" }}
                            />{" "}
                            <span style={{ fontSize: "13px" }}>{itemNumbers}</span>
                        </a>
                    </li>
                </ul>
                {/* <div className="header__cart__price">
                    item: <span>$150.00</span>
                </div> */}
            </div>
            <OffCanvasCartComponent show={show} handleclose={handleClose} placement={"end"} />
        </>
    );
};

export default CartItemComponent;

import React, { FC } from "react";
import Link from "next/link";

const LogoComponent: FC = () => {
    return (
        <>
            <div className="header__logo">
                <Link href="/">
                    <a className="logo">
                        <h2 className="fw-bold">DEVCAMP</h2>
                    </a>
                </Link>
            </div>
        </>
    );
};

export default LogoComponent;

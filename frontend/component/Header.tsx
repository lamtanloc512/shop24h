import { Fragment, useEffect } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import Routes from "./RoutePaths/Routes";
import HambergerMenuComponent from "./Header/HambergerMenuComponent";
import HeaderTopComponent from "./Header/HeaderTopComponent";
import LogoComponent from "./Header/LogoComponent";
import CartItemComponent from "./Header/CartItemComponent";

const Header = () => {
    const router = useRouter();
    useEffect(() => {
        $("#preloder").fadeOut("slow");
        $(".loader").delay(200).fadeIn("slow");
        return () => {
            $(".loader").off();
            $("#preloder").off();
        };
    }, []);

    return (
        <Fragment>
            <div id="preloder">
                <div className="loader" />
            </div>
            <HambergerMenuComponent />
            <header className="header">
                <HeaderTopComponent />
                <div className="container">
                    <div className="row">
                        <div className="col-lg-3">
                            <LogoComponent />
                        </div>
                        <div className="col-lg-8">
                            <nav className="header__menu">
                                <ul>
                                    {Routes.map((bI, index) => (
                                        <li
                                            key={index}
                                            className={
                                                router.asPath == bI.path
                                                    ? "header__menu__item active"
                                                    : "header__menu__item"
                                            }
                                        >
                                            <Link href={bI.path}>{bI.label}</Link>
                                        </li>
                                    ))}
                                </ul>
                            </nav>
                        </div>
                        <div className="col">
                            <CartItemComponent />
                        </div>
                    </div>
                    <div className="humberger__open">
                        <i className="fa fa-bars" />
                    </div>
                </div>
            </header>
        </Fragment>
    );
};

export default Header;

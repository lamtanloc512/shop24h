interface IRoutes {
    path: string;
    label: string;
}

const Routes: IRoutes[] = [
    { path: "/", label: "Home" },
    { path: "/shop/products", label: "Shop" },
    { path: "/cart", label: "Cart" },
    { path: "/checkout", label: "Checkout" },
    { path: "/contact", label: "Contact" },
];
export default Routes;

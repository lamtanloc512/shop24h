import { NextPage } from 'next';
import React from 'react';
import Footer from './Footer';
import Header from './Header';
import CssPlugins from './CssPlugins';
import JsPlugins from './JsPlugins';

interface Props {
    children: React.ReactNode;
}

const Layout = ({ children }: Props) => {
    return (
        <>
            <CssPlugins />
            <Header />
            {children}
            <Footer />
            <JsPlugins />
        </>
    );
};

export default Layout;

import React, { FC } from "react";

interface IPagination {
    pageNumber: number;
    totalPage: number;
    clickToChangePageNumber: (pageNumber: number) => void;
    position?: string;
}

const Pagination: FC<IPagination> = ({
    pageNumber,
    totalPage,
    clickToChangePageNumber,
    position,
}) => {
    // tao 2 bien xac dinh firtpage va lastpage;
    const isFirst: boolean = pageNumber === 0;
    const isLast: boolean = pageNumber === totalPage - 1;

    //tao mang chua page number
    let gPageNumbers = [];
    if (totalPage) {
        for (let index = 0; index < totalPage; index++) {
            gPageNumbers.push(index);
        }
    }
    position;
    return (
        <>
            <div
                className={
                    position != undefined
                        ? `product__pagination ${position}`
                        : "product__pagination"
                }
            >
                {isFirst ? (
                    <></>
                ) : (
                    <a
                        className="page__number__next"
                        onClick={() => clickToChangePageNumber(pageNumber - 1)}
                        style={{ textAlign: "center", verticalAlign: "baseline" }}
                    >
                        <i className="fa fa-long-arrow-left" />
                    </a>
                )}
                {gPageNumbers.slice(pageNumber, pageNumber + 3).map((bI) => {
                    return (
                        <a
                            className={pageNumber == bI ? "page__number active" : "page__number"}
                            data-index={bI}
                            key={bI}
                            onClick={() => clickToChangePageNumber(bI)}
                            style={{ textAlign: "center", verticalAlign: "baseline" }}
                        >
                            {bI + 1}
                        </a>
                    );
                })}
                {isLast ? (
                    <></>
                ) : (
                    <a
                        className="page__number__next"
                        onClick={() => clickToChangePageNumber(pageNumber + 1)}
                        style={{ textAlign: "center", verticalAlign: "baseline" }}
                    >
                        <i className="fa fa-long-arrow-right" />
                    </a>
                )}
            </div>
        </>
    );
};

export default Pagination;

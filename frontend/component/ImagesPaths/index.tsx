const ImagePaths = [
    '/img/featured/feature-1.jpg',
    '/img/featured/feature-2.jpg',
    '/img/featured/feature-3.jpg',
    '/img/featured/feature-4.jpg',
    '/img/featured/feature-5.jpg',
    '/img/featured/feature-6.jpg',
    '/img/featured/feature-7.jpg',
    '/img/featured/feature-8.jpg',
];

export default ImagePaths;

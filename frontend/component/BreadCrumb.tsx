import React, { FC } from "react";
import Link from "next/link";
import { useRouter } from "next/router";

interface BreadCrumbType {
    title?: string;
}

const BreadCrumb: FC<BreadCrumbType> = ({ title }) => {
    const router = useRouter();
    // console.log(router);
    return (
        <section
            className="breadcrumb-section"
            style={{
                backgroundImage: `url("/img/banner/banner-1.jpg")`,
                backgroundRepeat: "no-repeat",
                backgroundSize: "cover",
            }}
        >
            <div className="container">
                <div className="row">
                    <div className="col-lg-12 text-center">
                        <div className="breadcrumb__text">
                            <h2 className="text-dark">{title}</h2>
                            <div className="breadcrumb__option">
                                {/* <Link href="/">
                                    <a className="text-dark">Home</a>
                                </Link>
                                <Link href={router.asPath}>
                                    <a className="text-dark">{router.asPath}</a>
                                </Link> */}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default BreadCrumb;

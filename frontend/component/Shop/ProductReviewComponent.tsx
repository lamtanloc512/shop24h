import React, { FC } from "react";

const ProductReviewComponent: FC = () => {
    return (
        <div className="product__details__rating">
            <i className="fa fa-star" />
            <i className="fa fa-star" />
            <i className="fa fa-star" />
            <i className="fa fa-star" />
            <i className="fa fa-star-half-o" />
            <span>(18 reviews)</span>
        </div>
    );
};

export default ProductReviewComponent;

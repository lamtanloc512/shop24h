import React from "react";

const ProductVariations = () => {
    return (
        <ul>
            <li>
                <b>Availability</b> <span>In Stock</span>
            </li>
            <li>
                <b>Shipping</b>{" "}
                <span>
                    01 day shipping. <samp>Free pickup today</samp>
                </span>
            </li>
            <li>
                <b>Weight</b> <span>0.5 kg</span>
            </li>
            <li>
                <b>Share on</b>
                <div className="share">
                    <a href="#">
                        <i className="fa fa-facebook" />
                    </a>
                    <a href="#">
                        <i className="fa fa-twitter" />
                    </a>
                    <a href="#">
                        <i className="fa fa-instagram" />
                    </a>
                    <a href="#">
                        <i className="fa fa-pinterest" />
                    </a>
                </div>
            </li>
        </ul>
    );
};

export default ProductVariations;

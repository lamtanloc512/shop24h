import React, { FC } from "react";

const ProductTabInfomationComponent: FC = () => {
    return (
        <ul className="nav nav-tabs" role="tablist">
            <li className="nav-item">
                <a
                    className="nav-link active"
                    data-toggle="tab"
                    href="#tabs-1"
                    role="tab"
                    aria-selected="true"
                >
                    Description
                </a>
            </li>
            <li className="nav-item">
                <a
                    className="nav-link"
                    data-toggle="tab"
                    href="#tabs-2"
                    role="tab"
                    aria-selected="false"
                >
                    Information
                </a>
            </li>
            <li className="nav-item">
                <a
                    className="nav-link"
                    data-toggle="tab"
                    href="#tabs-3"
                    role="tab"
                    aria-selected="false"
                >
                    Reviews
                    {/* <span>(1)</span> */}
                </a>
            </li>
        </ul>
    );
};

export default ProductTabInfomationComponent;

import React, { FC, useState } from "react";

interface ProductDetail {
    addtocart: () => void;
}

const ProductQuantityComponent: FC<ProductDetail> = ({ addtocart }) => {
    const [amount, setAmount] = useState<number>(1);
    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setAmount(parseInt(e.target.value));
    };
    return (
        <>
            <div className="product__details__quantity">
                <div className="quantity">
                    <div className="pro-qty">
                        {/* <span className="dec qtybtn"> */}
                        <span
                            className="dec qtybtn btn btn-sm btn-default m-0 p-0"
                            onClick={() => {
                                if (amount >= 2) {
                                    return setAmount(amount - 1);
                                }
                            }}
                        >
                            -
                        </span>
                        <input type="number" value={amount} onChange={handleChange} />

                        <span
                            className="inc qtybtn btn btn-sm btn-default m-0 p-0"
                            onClick={() => setAmount(amount + 1)}
                        >
                            +
                        </span>
                    </div>
                </div>
            </div>
            <a className="primary-btn" onClick={addtocart}>
                ADD TO CARD
            </a>
            <a className="heart-icon">
                <span className="icon_heart_alt" />
            </a>
        </>
    );
};

export default ProductQuantityComponent;

import React, { FC } from "react";

interface ProductInfo {
    longDescription?: string | null;
    additionalDescription?: string | null;
}

const ProductInfomationSection: FC<ProductInfo> = ({ longDescription, additionalDescription }) => {
    return (
        <div className="tab-content">
            <div className="tab-pane active" id="tabs-1" role="tabpanel">
                <div className="product__details__tab__desc">
                    <h6>Products Infomation</h6>
                    <p>{longDescription}</p>
                </div>
            </div>
            <div className="tab-pane" id="tabs-2" role="tabpanel">
                <div className="product__details__tab__desc">
                    <h6>Additional Infomation </h6>
                    <p>{additionalDescription}</p>
                </div>
            </div>
            <div className="tab-pane" id="tabs-3" role="tabpanel">
                <div className="product__details__tab__desc">
                    <h6>Reviews</h6>
                    <p>
                        Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.
                        Pellentesque in ipsum id orci porta dapibus. Proin eget tortor risus.
                        Vivamus suscipit tortor eget felis porttitor volutpat. Vestibulum ac diam
                        sit amet quam vehicula elementum sed sit amet dui. Donec rutrum congue leo
                        eget malesuada. Vivamus suscipit tortor eget felis porttitor volutpat.
                        Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Praesent
                        sapien massa, convallis a pellentesque nec, egestas non nisi. Vestibulum ac
                        diam sit amet quam vehicula elementum sed sit amet dui. Vestibulum ante
                        ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;
                        Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula.
                        Proin eget tortor risus.
                    </p>
                </div>
            </div>
        </div>
    );
};

export default ProductInfomationSection;

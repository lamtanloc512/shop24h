import type { AppProps } from "next/app";
import Layout from "../component/Layout";
// import "bootstrap/dist/css/bootstrap.min.css";
import "../public/css/style.css";
import CartContextProvider from "../context/CartContextProvider";
import NextNProgress from "nextjs-progressbar";

function MyApp({ Component, pageProps }: AppProps) {
    return (
        <CartContextProvider>
            <NextNProgress />
            <Layout>
                <Component {...pageProps} />
            </Layout>
        </CartContextProvider>
    );
}

export default MyApp;

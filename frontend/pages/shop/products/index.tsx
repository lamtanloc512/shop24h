import Head from "next/head";
import Image from "next/image";
import { GetStaticProps } from "next/types";
import React, { FC, Fragment, useState, useEffect } from "react";
import BreadCrumb from "../../../component/BreadCrumb";
import ProductItem from "../../../component/ProductItem";
import { ProductData } from "../../../utils/interface";
import Pagination from "./../../../component/Pagination";

interface ShopPageInterface {
    payload?: ProductData[];
    totalPages: number;
}

const Shop: FC<ShopPageInterface> = ({ payload, totalPages }) => {
    const [productDataState, setProductDataState] = useState<any>(payload);
    const [loading, setLoading] = useState<boolean>(false);
    const [postPerpage, setPostPerpage] = useState<number>(12);
    const [pageNumber, setPageNumber] = useState<number>(0);
    const [totalPageNumber] = useState<number>(totalPages ? totalPages : 10);

    useEffect(() => {
        const loadProductWithPaging = async () => {
            const vRequest = await fetch(
                `http://localhost:9000/product/pagination?index=${pageNumber}&per_page=${postPerpage}`,
                {
                    method: "GET",
                    headers: {
                        "Content-type": "application/json;charset=UTF-8",
                    },
                },
            );
            if (vRequest.ok) {
                const vResponse: any = await vRequest.json();

                const newPayload: ProductData[] = vResponse.content.map((bI: any) => {
                    return {
                        ...bI,
                        productMainImg: {
                            ...bI.productMainImg,
                            imgPermalink: bI.productMainImg.imgPermalink.replace(
                                "C:\\Users\\Admin\\Desktop\\imgStore\\",
                                "http://192.168.1.6:8080/",
                            ),
                        },
                    };
                });
                setProductDataState(newPayload);
                setLoading(false);
            } else {
                console.log(vRequest.text());
            }
        };
        if (loading) {
            loadProductWithPaging();
        }
    }, [loading]);

    //tao mang chua page number
    let gPageNumbers = [];
    if (totalPageNumber) {
        for (let index = 0; index < totalPageNumber; index++) {
            gPageNumbers.push(index);
        }
    }

    //Trigger load khi an nut chuyen page number
    //tao function thuc thi su kien an chuyen page
    const clickToChangePageNumber = (pagenumber: number) => {
        setLoading(true);
        if (pagenumber < 0 || pagenumber === 0) {
            setPageNumber(0);
        } else {
            setPageNumber(pagenumber);
        }
    };

    return (
        <Fragment>
            <Head>
                <title>Shop Page</title>
            </Head>
            <BreadCrumb title="Shop" />
            <section className="product spad">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-3 col-md-5">
                            <div className="sidebar">
                                <div className="sidebar__item">
                                    <h4>Categories</h4>
                                    <ul>
                                        <li>
                                            <a href="#">Apple iPhone</a>
                                        </li>
                                        <li>
                                            <a href="#">Apple Macbook</a>
                                        </li>
                                        <li>
                                            <a href="#">Apple iPad</a>
                                        </li>
                                        <li>
                                            <a href="#">Apple SmartWatch</a>
                                        </li>
                                        <li>
                                            <a href="#">Sony Camera</a>
                                        </li>
                                        <li>
                                            <a href="#">JBL Headphone</a>
                                        </li>
                                        <li>
                                            <a href="#">JBL Speaker</a>
                                        </li>
                                        <li>
                                            <a href="#">Xbox One</a>
                                        </li>
                                        <li>
                                            <a href="#"> B &amp; O Headphone</a>
                                        </li>
                                        <li>
                                            <a href="#">Monitor</a>
                                        </li>
                                        <li>
                                            <a href="#">Oatmeal</a>
                                        </li>
                                    </ul>
                                </div>
                                <div className="sidebar__item">
                                    <h4>Price</h4>
                                    <div className="price-range-wrap">
                                        <div
                                            className="price-range ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content"
                                            data-min={10}
                                            data-max={540}
                                        >
                                            <div className="ui-slider-range ui-corner-all ui-widget-header" />
                                            <span
                                                tabIndex={0}
                                                className="ui-slider-handle ui-corner-all ui-state-default"
                                            />
                                            <span
                                                tabIndex={0}
                                                className="ui-slider-handle ui-corner-all ui-state-default"
                                            />
                                        </div>
                                        <div className="range-slider">
                                            <div className="price-input">
                                                <input type="text" id="minamount" />
                                                <input type="text" id="maxamount" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="sidebar__item sidebar__item__color--option">
                                    <h4>Colors</h4>
                                    <div className="sidebar__item__color sidebar__item__color--white">
                                        <label htmlFor="white">
                                            White
                                            <input type="radio" id="white" />
                                        </label>
                                    </div>
                                    <div className="sidebar__item__color sidebar__item__color--gray">
                                        <label htmlFor="gray">
                                            Gray
                                            <input type="radio" id="gray" />
                                        </label>
                                    </div>
                                    <div className="sidebar__item__color sidebar__item__color--red">
                                        <label htmlFor="red">
                                            Red
                                            <input type="radio" id="red" />
                                        </label>
                                    </div>
                                    <div className="sidebar__item__color sidebar__item__color--black">
                                        <label htmlFor="black">
                                            Black
                                            <input type="radio" id="black" />
                                        </label>
                                    </div>
                                    <div className="sidebar__item__color sidebar__item__color--blue">
                                        <label htmlFor="blue">
                                            Blue
                                            <input type="radio" id="blue" />
                                        </label>
                                    </div>
                                    <div className="sidebar__item__color sidebar__item__color--green">
                                        <label htmlFor="green">
                                            Green
                                            <input type="radio" id="green" />
                                        </label>
                                    </div>
                                </div>
                                <div className="sidebar__item">
                                    <h4>Popular Size</h4>
                                    <div className="sidebar__item__size">
                                        <label htmlFor="large">
                                            Large
                                            <input type="radio" id="large" />
                                        </label>
                                    </div>
                                    <div className="sidebar__item__size">
                                        <label htmlFor="medium">
                                            Medium
                                            <input type="radio" id="medium" />
                                        </label>
                                    </div>
                                    <div className="sidebar__item__size">
                                        <label htmlFor="small">
                                            Small
                                            <input type="radio" id="small" />
                                        </label>
                                    </div>
                                    <div className="sidebar__item__size">
                                        <label htmlFor="tiny">
                                            Tiny
                                            <input type="radio" id="tiny" />
                                        </label>
                                    </div>
                                </div>
                                <div className="sidebar__item">
                                    <div className="latest-product__text">
                                        <h4>Latest Products</h4>
                                        <div className="latest-product__slider owl-carousel">
                                            <div className="latest-prdouct__slider__item">
                                                <a href="#" className="latest-product__item">
                                                    <div className="latest-product__item__pic">
                                                        <Image
                                                            src="/img/latest-product/lp-1.jpg"
                                                            layout="fill"
                                                        />
                                                    </div>
                                                    <div className="latest-product__item__text">
                                                        <h6>Crab Pool Security</h6>
                                                        <span>$30.00</span>
                                                    </div>
                                                </a>
                                                <a href="#" className="latest-product__item">
                                                    <div className="latest-product__item__pic">
                                                        <Image
                                                            src="/img/latest-product/lp-2.jpg"
                                                            layout="fill"
                                                        />
                                                    </div>
                                                    <div className="latest-product__item__text">
                                                        <h6>Crab Pool Security</h6>
                                                        <span>$30.00</span>
                                                    </div>
                                                </a>
                                                <a href="#" className="latest-product__item">
                                                    <div className="latest-product__item__pic">
                                                        <Image
                                                            src="/img/latest-product/lp-3.jpg"
                                                            layout="fill"
                                                        />
                                                    </div>
                                                    <div className="latest-product__item__text">
                                                        <h6>Crab Pool Security</h6>
                                                        <span>$30.00</span>
                                                    </div>
                                                </a>
                                            </div>
                                            <div className="latest-prdouct__slider__item">
                                                <a href="#" className="latest-product__item">
                                                    <div className="latest-product__item__pic">
                                                        <Image
                                                            src="/img/latest-product/lp-1.jpg"
                                                            layout="fill"
                                                        />
                                                    </div>
                                                    <div className="latest-product__item__text">
                                                        <h6>Crab Pool Security</h6>
                                                        <span>$30.00</span>
                                                    </div>
                                                </a>
                                                <a href="#" className="latest-product__item">
                                                    <div className="latest-product__item__pic">
                                                        <Image
                                                            src="/img/latest-product/lp-2.jpg"
                                                            layout="fill"
                                                        />
                                                    </div>
                                                    <div className="latest-product__item__text">
                                                        <h6>Crab Pool Security</h6>
                                                        <span>$30.00</span>
                                                    </div>
                                                </a>
                                                <a href="#" className="latest-product__item">
                                                    <div className="latest-product__item__pic">
                                                        <Image
                                                            src="/img/latest-product/lp-3.jpg"
                                                            layout="fill"
                                                        />
                                                    </div>
                                                    <div className="latest-product__item__text">
                                                        <h6>Crab Pool Security</h6>
                                                        <span>$30.00</span>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-9 col-md-7">
                            <div className="filter__item">
                                <div className="row">
                                    <div className="col-lg-4 col-md-5">
                                        <div className="filter__sort">
                                            <span className="mx-3">Sort By</span>
                                            <select>
                                                <option value={0}>Default</option>
                                                <option value={0}>Default</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div className="col-lg-4 col-md-4">
                                        {/* <div className="filter__found">
                                            <h6>
                                                <span>16</span> Products found
                                            </h6>
                                        </div> */}
                                    </div>
                                    <div className="col-lg-4 col-md-3">
                                        <div className="filter__option">
                                            <span className="icon_grid-2x2" />
                                            <span className="icon_ul" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                {productDataState != undefined || productDataState != null ? (
                                    productDataState.map((productElement: ProductData) => {
                                        return (
                                            <div
                                                key={productElement.id}
                                                className="col-6 col-lg-3 col-md-4 mix"
                                            >
                                                <div className="featured__item">
                                                    <ProductItem payload={productElement} />
                                                </div>
                                            </div>
                                        );
                                    })
                                ) : (
                                    <h2 className="text-center">No products founds!</h2>
                                )}
                            </div>
                            <Pagination
                                pageNumber={pageNumber}
                                totalPage={totalPageNumber}
                                clickToChangePageNumber={clickToChangePageNumber}
                            />
                        </div>
                    </div>
                </div>
            </section>
        </Fragment>
    );
};

export default Shop;

export const getStaticProps: GetStaticProps = async () => {
    let vResult: ProductData[] | undefined = [];

    const vRequest = await fetch("http://localhost:9000/product/pagination", {
        method: "GET",
        headers: {
            "Content-type": "application/json;charset=UTF-8",
        },
    });
    let vResponse = null;
    if (vRequest.ok) {
        vResponse = await vRequest.json();
        vResult = vResponse.content.map((bI: any) => {
            return {
                ...bI,
                productMainImg: {
                    ...bI.productMainImg,
                    imgPermalink: bI.productMainImg.imgPermalink.replace(
                        "C:\\Users\\Admin\\Desktop\\imgStore\\",
                        "http://192.168.1.6:8080/",
                    ),
                },
            };
        });
    }
    return {
        props: {
            payload: vResult,
            totalPages: vResponse != null ? vResponse.totalPages : null,
        },
    };
};

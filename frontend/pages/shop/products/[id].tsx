import React, { Fragment, useState, useEffect, useContext, FC } from "react";
import BreadCrumb from "../../../component/BreadCrumb";
import Image from "next/image";
import { ProductInitialState } from "./../../../reducer/ProductReducer";
import { useRouter } from "next/router";
import { Product, ProductData } from "../../../utils/interface";
import ProductVariations from "../../../component/Shop/ProductVariations";
import ProductQuantityComponent from "../../../component/Shop/ProductQuantityComponent";
import ProductInfomationSection from "../../../component/Shop/ProductInfomationSection";
import ProductRelatedSectionComponent from "../../../component/Shop/ProductRelatedSectionComponent";
import ProductTabInfomationComponent from "../../../component/Shop/ProductTabInfomationComponent";
import ProductReviewComponent from "../../../component/Shop/ProductReviewComponent";
import Head from "next/head";
import { CartContext } from "../../../context/CartContextProvider";
import { CartItem } from "../../../reducer/ShoppingCartReducer";
import Link from "next/link";

interface ProductDetail {
    addtocart: () => void;
    longDescription?: string;
    additionalDescription?: string;
}

const ProductDetail: FC<ProductDetail> = () => {
    const router: any = useRouter();
    const [loadTrigger, setLoadTrigger] = useState<boolean>(false);
    const [contentLoaded, setContentLoaded] = useState<boolean>(false);
    const [productDetailState, setProductDetailState] = useState<any>(null);
    const { addToCartAction } = useContext(CartContext);
    //add to cart when click icon cart
    const addToCart = (): void => {
        const vPayload: CartItem = {
            id: productDetailState.id,
            name: productDetailState.productName,
            price: productDetailState.price,
            qty: 1,
            imgUrl: productDetailState.productMainImg.imgPermalink,
        };
        addToCartAction(vPayload);
        // console.log(productDetailState);
    };

    // find product id in start state and pass push to a object
    useEffect(() => {
        const productId: number = parseInt(router.query.id);
        const fetchProductById = async () => {
            if (productId) {
                const vRequest = await fetch(`http://localhost:9000/product/${productId}`, {
                    method: "GET",
                    headers: {
                        "Content-type": "application/json;charset=UTF-8",
                    },
                });

                if (vRequest.ok) {
                    const vResponse = await vRequest.json();
                    let vResult: ProductData = {
                        ...vResponse,
                        productMainImg: {
                            ...vResponse.productMainImg,
                            imgPermalink: vResponse.productMainImg.imgPermalink.replace(
                                "C:\\Users\\Admin\\Desktop\\imgStore\\",
                                "http://192.168.1.6:8080/",
                            ),
                        },
                    };
                    // let vResult: Product = {
                    //     id: vResponse.id,
                    //     title: vResponse.productName,
                    //     price: vResponse.price,
                    //     description: vResponse.productInfomation.shortDescription,
                    //     imgUrl: vResponse.productMainImg.imgPermalink.replace(
                    //         "C:\\Users\\Admin\\Desktop\\imgStore\\",
                    //         "http://192.168.1.6:8080/",
                    //     ),
                    // };
                    setProductDetailState(vResult);
                }
            }
        };
        fetchProductById();
    }, [router.query.id]);

    useEffect(() => {
        if (productDetailState !== null) {
            setContentLoaded(true);
            // console.log(productDetailState);
        }
    }, [productDetailState]);

    return (
        <Fragment>
            <Head>
                <title>Product Detail</title>
            </Head>
            <BreadCrumb title={contentLoaded ? productDetailState.productName : "Amazing Shop"} />

            <section className="product-details spad">
                <div className="container">
                    <div className="row mb-5">
                        <div className="col-lg-12 d-flex">
                            <div className="shoping__cart__btns mx-2">
                                <Link href="/">
                                    <a className="primary-btn" style={{ cursor: "pointer" }}>
                                        BACK TO HOME
                                    </a>
                                </Link>
                            </div>
                            <div className="shoping__cart__btns mx-2">
                                <Link href="/shop/products">
                                    <a
                                        className="primary-btn cart-btn"
                                        style={{ cursor: "pointer" }}
                                    >
                                        CONTINUE SHOPPING
                                    </a>
                                </Link>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-lg-6 col-md-6">
                            <div className="product__details__pic h-100 w-100">
                                <div className="product__details__pic__item h-100 w-100 d-flex justify-content-center align-items-center">
                                    {contentLoaded ? (
                                        <Image
                                            className="product__details__pic__item--large"
                                            src={productDetailState.productMainImg.imgPermalink}
                                            alt="main__img__product"
                                            layout="fixed"
                                            objectFit="contain"
                                            width={400}
                                            height={400}
                                            quality={100}
                                        />
                                    ) : (
                                        <Image
                                            src="/img/dummy.jpg"
                                            alt="dummy__img__product"
                                            layout="fixed"
                                            objectFit="contain"
                                            width={400}
                                            height={400}
                                            quality={100}
                                        />
                                    )}
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-6 col-md-6">
                            <div className="product__details__text">
                                <h3>{contentLoaded ? productDetailState.productName : ""}</h3>
                                <ProductReviewComponent />
                                <div className="product__details__price">
                                    {contentLoaded ? `$${productDetailState.price}` : "$"}
                                </div>
                                <p>
                                    {contentLoaded
                                        ? productDetailState.productInfomation.shortDescription
                                        : "No content here"}
                                </p>
                                <ProductQuantityComponent addtocart={addToCart} />
                                <ProductVariations />
                            </div>
                        </div>
                        <div className="col-lg-12">
                            <div className="product__details__tab">
                                <ProductTabInfomationComponent />
                                <ProductInfomationSection
                                    longDescription={
                                        productDetailState != null
                                            ? productDetailState.productInfomation.longDescription
                                            : ""
                                    }
                                    additionalDescription={
                                        productDetailState != null
                                            ? productDetailState.productInfomation
                                                  .additionalDescription
                                            : ""
                                    }
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            {/* <ProductRelatedSectionComponent /> */}
        </Fragment>
    );
};

export default ProductDetail;

import React, { useState, MouseEvent } from 'react';

const Test = () => {
    const [amount, setAmount] = useState<number>(1);

    const handleIncrease = (e: MouseEvent<HTMLElement>) => {
        console.log('hello');
        console.log(e);
    };

    return (
        <div>
            <input type="number" value={amount} readOnly />
            <button className="btn btn-primary" onClick={handleIncrease}>
                +
            </button>
        </div>
    );
};

export default Test;

import type { GetStaticProps, NextPage } from "next";
import Head from "next/head";
import { Fragment, useEffect, useState, useContext } from "react";
import FeaturedProductSection from "../sections/FeaturedProductSection";
import HeroSection from "../sections/HeroSection";
import CategoriesSection from "../sections/CategoriesSection";
import BannerSection from "../sections/BannerSection";
import BlogSection from "../sections/BlogSection";
import LastestProductSection from "../sections/LastestProductSection";
import { ProductData, Product } from "../utils/interface";
import { CartContext } from "./../context/CartContextProvider";
import $ from "jquery";

interface HomePageInterface {
    payload?: ProductData[];
    totalPages: number;
    toastr?: any;
    success: () => any;
}

const Home: NextPage<HomePageInterface> = ({ payload, totalPages, toastr, success }) => {
    const [productDataState, setProductDataState] = useState<any>(payload);
    const [loading, setLoading] = useState<boolean>(false);
    const [postPerpage, setPostPerpage] = useState<number>(8);
    const [pageNumber, setPageNumber] = useState<number>(0);
    const [totalPageNumber] = useState<number>(totalPages ? totalPages : 10);
    const [notification, setNotification] = useState<boolean>(false);

    //lay state tu gio hang, xac dinh xem khi nao gio hang thay doi so luowng se thoong bao
    const { cartList } = useContext(CartContext);

    // useEffect(() => {
    //     $(function () {
    //         if (notification) toastr.success("Create product successfully!!!");
    //     });
    // }, [notification]);

    useEffect(() => {
        setNotification(true);
    }, [cartList]);

    useEffect(() => {
        const loadProductWithPaging = async () => {
            const vRequest = await fetch(
                `http://localhost:9000/product/pagination?index=${pageNumber}&per_page=${postPerpage}`,
                {
                    method: "GET",
                    headers: {
                        "Content-type": "application/json;charset=UTF-8",
                    },
                },
            );
            if (vRequest.ok) {
                const vResponse: any = await vRequest.json();

                const newPayload: ProductData[] = vResponse.content.map((bI: any) => {
                    return {
                        ...bI,
                        productMainImg: {
                            ...bI.productMainImg,
                            imgPermalink: bI.productMainImg.imgPermalink.replace(
                                "C:\\Users\\Admin\\Desktop\\imgStore\\",
                                "http://192.168.1.6:8080/",
                            ),
                        },
                    };
                });
                setProductDataState(newPayload);
                setLoading(false);
            } else {
                console.log(vRequest.text());
            }
        };
        if (loading) {
            loadProductWithPaging();
        }
    }, [loading]);

    //tao mang chua page number
    let gPageNumbers = [];
    if (totalPageNumber) {
        for (let index = 0; index < totalPageNumber; index++) {
            gPageNumbers.push(index);
        }
    }

    //Trigger load khi an nut chuyen page number
    //tao function thuc thi su kien an chuyen page
    const clickToChangePageNumber = (pagenumber: number) => {
        setLoading(true);
        if (pagenumber < 0 || pagenumber === 0) {
            setPageNumber(0);
        } else {
            setPageNumber(pagenumber);
        }
    };

    useEffect(() => {
        if (payload != null) {
            // console.log(payload);
        }
    }, [payload]);

    return (
        <Fragment>
            <Head>
                <title>Pro Shop</title>
                <meta name="description" content="Ecommerce electronic shop" />
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <div>
                <HeroSection />
                <CategoriesSection />
                <BannerSection />
                <FeaturedProductSection
                    payload={productDataState}
                    totalPages={totalPageNumber}
                    pageNumber={pageNumber}
                    clickToChangePageNumber={clickToChangePageNumber}
                />
                {/* <LastestProductSection /> */}
                {/* <BlogSection /> */}
            </div>
        </Fragment>
    );
};

export default Home;

export const getStaticProps: GetStaticProps = async () => {
    let vResult: ProductData[] | undefined = [];

    const vRequest = await fetch(`http://localhost:9000/product/pagination?per_page=8`, {
        method: "GET",
        headers: {
            "Content-type": "application/json;charset=UTF-8",
        },
    });
    let vResponse = null;
    if (vRequest.ok) {
        vResponse = await vRequest.json();
        vResult = vResponse.content.map((bI: any) => {
            return {
                ...bI,
                productMainImg: {
                    ...bI.productMainImg,
                    imgPermalink: bI.productMainImg.imgPermalink.replace(
                        "C:\\Users\\Admin\\Desktop\\imgStore\\",
                        "http://192.168.1.6:8080/",
                    ),
                },
            };
        });
    }
    return {
        props: {
            payload: vResult,
            totalPages: vResponse != null ? vResponse.totalPages : null,
        },
    };
};

import React, { Fragment, useContext, useState, useEffect } from "react";
import { NextPage } from "next";
import BreadCrumb from "../component/BreadCrumb";
import Head from "next/head";
import { CartContext } from "./../context/CartContextProvider";
import { CartItem } from "../reducer/ShoppingCartReducer";
import Image from "next/image";
import Link from "next/link";

const Cart: NextPage = () => {
    const { cartList, increaseQtyCart, decreaseQtyCart, deleteProductCart, changeQtyProduct } =
        useContext(CartContext);
    const [totalPrice, setTotalPrice] = useState<number>(0);

    // sử dụng state này để giải quyết lỗi serverside -> clientside
    const [mounted, setMounted] = useState(false);
    useEffect(() => {
        setMounted(true);
    }, []);

    // set tổng giá tiền
    useEffect(() => {
        console.log(cartList);

        let total = 0;
        for (let index = 0; index < cartList.length; index++) {
            total = total + cartList[index].qty * parseInt(cartList[index].price);
        }
        setTotalPrice(total);
    }, [cartList]);

    // các action tăng giảm, sửa , xoá state
    const handleChangeQty = (e: React.ChangeEvent<HTMLInputElement>) => {
        const dataChangeElementJson = e.currentTarget.dataset.element;
        let payload = {};
        if (dataChangeElementJson) {
            payload = {
                ...JSON.parse(dataChangeElementJson),
                qty: e.target.value,
            };
        }
        changeQtyProduct(payload);
    };

    const increaseQty = (e: React.SyntheticEvent<HTMLElement>) => {
        // console.log(e.currentTarget.dataset.element);
        const dataElementJson = e.currentTarget.dataset.element;
        let payload = {};
        if (dataElementJson) {
            payload = JSON.parse(dataElementJson);
        }
        // console.log(payload);
        increaseQtyCart(payload);
    };

    const decreaseQty = (e: React.SyntheticEvent<HTMLElement>) => {
        // console.log(e.currentTarget.dataset.element);
        const dataElementJson = e.currentTarget.dataset.element;
        let payload = {};
        if (dataElementJson) {
            payload = JSON.parse(dataElementJson);
        }
        // console.log(payload);
        decreaseQtyCart(payload);
    };

    const handleDelete = (e: React.SyntheticEvent<HTMLElement>) => {
        // console.log(e.currentTarget.dataset.element);
        const dataDeleteElementJson = e.currentTarget.dataset.element;
        let payload = {};
        if (dataDeleteElementJson) {
            payload = JSON.parse(dataDeleteElementJson);
        }
        deleteProductCart(payload);
    };

    return (
        <Fragment>
            <Head>
                <title>Cart</title>
            </Head>
            <BreadCrumb title="Cart" />
            <section className="shoping-cart spad">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-12">
                            <div className="shoping__cart__table">
                                <table>
                                    <thead>
                                        <tr>
                                            <th className="shoping__product">Products</th>
                                            <th>Price</th>
                                            <th>Quantity</th>
                                            <th>Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {mounted
                                            ? cartList.map((cartItem: CartItem, index: number) => {
                                                  return (
                                                      <tr key={index}>
                                                          <td className="shoping__cart__item d-flex justify-content-start align-items-center">
                                                              {cartItem.imgUrl ? (
                                                                  <Image
                                                                      src={cartItem.imgUrl}
                                                                      width={100}
                                                                      height={100}
                                                                  />
                                                              ) : (
                                                                  <Image
                                                                      src="/img/dummy.jpg"
                                                                      height={100}
                                                                      width={100}
                                                                  />
                                                              )}

                                                              <h5 className="fw-bold mx-5">
                                                                  {cartItem.name}
                                                              </h5>
                                                          </td>
                                                          <td className="shoping__cart__price">
                                                              ${cartItem.price}
                                                          </td>
                                                          <td className="shoping__cart__quantity">
                                                              <div className="quantity">
                                                                  <div className="pro-qty">
                                                                      <span
                                                                          className="inc qtybtn btn btn-sm btn-default m-0 p-0"
                                                                          onClick={decreaseQty}
                                                                          data-element={JSON.stringify(
                                                                              cartItem,
                                                                          )}
                                                                      >
                                                                          -
                                                                      </span>
                                                                      <input
                                                                          type="text"
                                                                          value={cartItem.qty}
                                                                          onChange={handleChangeQty}
                                                                          data-element={JSON.stringify(
                                                                              cartItem,
                                                                          )}
                                                                      />
                                                                      <span
                                                                          className="inc qtybtn btn btn-sm btn-default m-0 p-0"
                                                                          onClick={increaseQty}
                                                                          data-element={JSON.stringify(
                                                                              cartItem,
                                                                          )}
                                                                      >
                                                                          +
                                                                      </span>
                                                                  </div>
                                                              </div>
                                                          </td>
                                                          <td className="shoping__cart__total">
                                                              $
                                                              {cartItem.qty *
                                                                  parseInt(cartItem.price)}
                                                          </td>
                                                          <td className="shoping__cart__item__close">
                                                              <span
                                                                  className="icon_close"
                                                                  onClick={handleDelete}
                                                                  data-element={JSON.stringify(
                                                                      cartItem,
                                                                  )}
                                                              />
                                                          </td>
                                                      </tr>
                                                  );
                                              })
                                            : null}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-lg-12">
                            <div className="shoping__cart__btns">
                                <Link href="/shop/products">
                                    <a
                                        className="primary-btn cart-btn"
                                        style={{ cursor: "pointer" }}
                                    >
                                        CONTINUE SHOPPING
                                    </a>
                                </Link>
                            </div>
                        </div>
                        <div className="col-lg-6">
                            <div className="shoping__continue">
                                <div className="shoping__discount">
                                    <h5>Discount Codes</h5>
                                    <form action="#">
                                        <input type="text" placeholder="Enter your coupon code" />
                                        <button type="submit" className="site-btn">
                                            APPLY COUPON
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-6">
                            <div className="shoping__checkout">
                                <h5>Cart Total</h5>
                                <ul>
                                    <li>
                                        Subtotal <span>${totalPrice}</span>
                                    </li>
                                    <li>
                                        Total <span>${totalPrice}</span>
                                    </li>
                                </ul>
                                <Link href={"/checkout"}>
                                    <a className="primary-btn" style={{ cursor: "pointer" }}>
                                        PROCEED TO CHECKOUT
                                    </a>
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </Fragment>
    );
};

export default Cart;

import React, { Fragment, useContext, useEffect, useState } from "react";
import { NextPage } from "next";
import BreadCrumb from "../component/BreadCrumb";
import Head from "next/head";
import { CartContext } from "./../context/CartContextProvider";
import { CartItem } from "../reducer/ShoppingCartReducer";

const Checkout: NextPage = () => {
    const { cartList } = useContext(CartContext);
    const [totalPrice, setTotalPrice] = useState<number>(0);
    // sử dụng state này để giải quyết lỗi serverside -> clientside
    const [mounted, setMounted] = useState(false);
    useEffect(() => {
        setMounted(true);
    }, []);

    // set tổng giá tiền
    useEffect(() => {
        let total = 0;

        for (let index = 0; index < cartList.length; index++) {
            total = total + cartList[index].qty * parseInt(cartList[index].price);
        }
        setTotalPrice(total);
    }, [cartList]);

    return (
        <Fragment>
            <Head>
                <title>Checkout</title>
            </Head>
            <BreadCrumb title="Checkout" />
            <section className="checkout spad">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-12">
                            <h6>
                                <span className="icon_tag_alt" /> Have a coupon?{" "}
                                <a href="#">Click here</a> to enter your code
                            </h6>
                        </div>
                    </div>
                    <div className="checkout__form">
                        <h4>Order Detail</h4>
                        <form action="#">
                            <div className="row">
                                <div className="col-lg-8 col-md-6">
                                    <div className="row">
                                        <div className="col-lg-6">
                                            <div className="checkout__input">
                                                <p>
                                                    Họ<span>*</span>
                                                </p>
                                                <input type="text" />
                                            </div>
                                        </div>
                                        <div className="col-lg-6">
                                            <div className="checkout__input">
                                                <p>
                                                    Tên<span>*</span>
                                                </p>
                                                <input type="text" />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-lg-6">
                                            <div className="checkout__input">
                                                <p>
                                                    Số điện thoại<span>*</span>
                                                </p>
                                                <input type="text" />
                                            </div>
                                        </div>
                                        <div className="col-lg-6">
                                            <div className="checkout__input">
                                                <p>
                                                    Email<span>*</span>
                                                </p>
                                                <input type="text" />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="checkout__input">
                                        <p>
                                            Địa chỉ<span>*</span>
                                        </p>
                                        <input
                                            type="text"
                                            placeholder="Địa chỉ"
                                            className="checkout__input__add"
                                        />
                                    </div>

                                    <div className="checkout__input">
                                        <p>Ghi chú</p>
                                        <textarea
                                            placeholder="Để lại lời nhắn và thông tin thêm của bạn."
                                            rows={5}
                                            className="w-100 p-2"
                                        />
                                    </div>
                                </div>
                                <div className="col-lg-4 col-md-6">
                                    <div className="checkout__order">
                                        <h4>Your Order</h4>
                                        <div className="checkout__order__products">
                                            Products <span>Total</span>
                                        </div>
                                        <ul>
                                            {mounted
                                                ? cartList.map(
                                                      (cartItem: CartItem, index: number) => {
                                                          return (
                                                              <li key={index}>
                                                                  {cartItem.name}
                                                                  <span>
                                                                      $
                                                                      {cartItem.qty *
                                                                          parseInt(cartItem.price)}
                                                                  </span>
                                                              </li>
                                                          );
                                                      }
                                                  )
                                                : ""}
                                        </ul>
                                        <div className="checkout__order__subtotal">
                                            Subtotal <span>${totalPrice}</span>
                                        </div>
                                        <div className="checkout__order__total">
                                            Total <span>${totalPrice}</span>
                                        </div>
                                        {/* <div className="checkout__input__checkbox">
                                            <label htmlFor="acc-or">
                                                Create an account?
                                                <input type="checkbox" id="acc-or" />
                                                <span className="checkmark" />
                                            </label>
                                        </div> */}
                                        <p>Choose your payment</p>
                                        <div className="checkout__input__checkbox">
                                            <label htmlFor="payment">
                                                Cod
                                                <input
                                                    type="checkbox"
                                                    id="payment"
                                                    name="payment"
                                                />
                                                <span className="checkmark" />
                                            </label>
                                        </div>
                                        <div className="checkout__input__checkbox">
                                            <label htmlFor="paypal">
                                                Momo
                                                <input type="checkbox" id="paypal" name="payment" />
                                                <span className="checkmark" />
                                            </label>
                                        </div>
                                        <button type="submit" className="site-btn">
                                            PLACE ORDER
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        </Fragment>
    );
};

export default Checkout;

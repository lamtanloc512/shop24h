export interface MenuPaths {
    title: string;
    url: string;
}

export interface Section {
    jsx: React.ReactNode;
}

export interface Product {
    id: number;
    title: string;
    price: string;
    description: string;
    imgUrl: string;
}

export type ProductData = {
    id: number;
    productName: string;
    productSlug: string;
    productCode: string;
    price: string;
    brand: {
        id: number;
        brandName: string;
        brandDescription: string;
        brandImgPath: string;
    };
    stock: {
        id: number;
        quantity: number;
    };
    voucher: any;
    productInfomation: {
        id: number;
        shortDescription: string;
        longDescription: string;
        additionalDescription: string;
    };
    productMainImg: {
        id: number;
        imgAlt: string;
        imgPermalink: string;
        type: string;
        imgUri: string;
    };
    gallery: any;
    productOpt: any;
    orderItem: any;
};

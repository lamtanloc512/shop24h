import { Product, ProductData } from "./../utils/interface";

interface ProductInitialState {
    payload: Product[];
}

export const ProductInitialState: ProductInitialState = {
    payload: [
        {
            id: 1,
            title: "Macbookpro M1 2022",
            description: "des",
            price: "1000",
            imgUrl: "/img/featured/feature-1.jpg",
        },
        {
            id: 2,
            title: "Sony Speaker",
            description: "des",
            price: "700",
            imgUrl: "/img/featured/feature-2.jpg",
        },
        {
            id: 3,
            title: "iPad Pro 2022",
            description: "des",
            price: "900",
            imgUrl: "/img/featured/feature-3.jpg",
        },
        {
            id: 4,
            title: "JBL Headphone",
            description: "des",
            price: "500",
            imgUrl: "/img/featured/feature-4.jpg",
        },
        {
            id: 5,
            title: "SmartWatch 2021",
            description: "des",
            price: "800",
            imgUrl: "/img/featured/feature-5.jpg",
        },
        {
            id: 6,
            title: "Sony Camera 2021",
            description: "des",
            price: "850",
            imgUrl: "/img/featured/feature-6.jpg",
        },
        {
            id: 7,
            title: "JBL ProSpeaker",
            description: "des",
            price: "750",
            imgUrl: "/img/featured/feature-7.jpg",
        },
        {
            id: 8,
            title: "Xbox One",
            description: "des",
            price: "650",
            imgUrl: "/img/featured/feature-8.jpg",
        },
    ],
};

export const ProductAction: any = {
    GET_ALL_PRODUCT: "GET_ALL_PRODUCT",
    GET_PRODUCT_BY_CATEGORY: "GET_PRODUCT_BY_CATEGORY",
};

export const ProductReducer = (state: ProductInitialState, action: typeof ProductAction) => {
    switch (action) {
        case ProductAction.GET_ALL_PRODUCT:
            console.log("getall");
            return state;
        default:
            break;
    }
};

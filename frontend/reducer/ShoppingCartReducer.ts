const CHANGE_CART_AMOUNT = "CHANGE_CART_AMOUNT";
const INCREASE_CART_AMOUNT = "INCREASE_CART_AMOUNT";
const DECREASE_CART_AMOUNT = "DECREASE_CART_AMOUNT";
const DELETE_CART_AMOUNT = "DELETE_CART_AMOUNT";
const ONCHANGE_CART_AMOUNT = "ONCHANGE_CART_AMOUNT";

export type CartItem = {
    id?: string | number;
    name?: string | number;
    qty: number;
    price: string;
    imgUrl: string;
};

export type cartStateType = {
    cartList: CartItem[];
};

export type cartActionType = {
    type:
        | typeof CHANGE_CART_AMOUNT
        | typeof INCREASE_CART_AMOUNT
        | typeof DECREASE_CART_AMOUNT
        | typeof DELETE_CART_AMOUNT
        | typeof ONCHANGE_CART_AMOUNT;
    payload: CartItem;
};

export const cartReducer: React.Reducer<cartStateType | any, cartActionType> = (
    state: cartStateType,
    action: cartActionType
) => {
    switch (action.type) {
        case CHANGE_CART_AMOUNT:
            let cartList = state.cartList;
            let cartItem = action.payload;
            let exist = cartList.find((item) => item.id === cartItem.id);

            if (exist) {
                return {
                    cartList: cartList.map((item) => {
                        if (item.id === cartItem.id)
                            return { ...item, qty: item.qty + cartItem.qty };
                        else return item;
                    }),
                };
            } else {
                return {
                    cartList: [...cartList, cartItem],
                };
            }

        case INCREASE_CART_AMOUNT:
            let elementIncreaseFound = state.cartList.find((item) => item.id === action.payload.id);
            if (elementIncreaseFound) {
                return {
                    cartList: state.cartList.map((item) => {
                        if (item.id === action.payload.id)
                            return { ...item, qty: action.payload.qty + 1 };
                        else return item;
                    }),
                };
            }
        case DECREASE_CART_AMOUNT:
            let elementDecreaseFound = state.cartList.find((item) => item.id === action.payload.id);
            if (elementDecreaseFound) {
                return {
                    cartList: state.cartList.map((item) => {
                        if (item.id === action.payload.id)
                            return {
                                ...item,
                                qty: action.payload.qty > 1 ? action.payload.qty - 1 : 1,
                            };
                        else return item;
                    }),
                };
            }
        case DELETE_CART_AMOUNT:
            return {
                cartList: state.cartList.filter((item) => item.id !== action.payload.id),
            };
        case ONCHANGE_CART_AMOUNT:
            return {
                cartList: state.cartList.map((item) => {
                    if (item.id === action.payload.id) return { ...item, qty: action.payload.qty };
                    else return item;
                }),
            };
        default: {
        }
    }
};

import Link from "next/link";
import Image from "next/image";

const BannerSection = () => {
    return (
        <div className="banner">
            <div className="container">
                <div className="row">
                    <div className="col-lg-6 col-md-6 col-12 my-2">
                        <div className="banner__pic position-relative">
                            <Image
                                src="/img/banner/banner-2.jpg"
                                width={450}
                                height={130}
                                layout="responsive"
                                quality={100}
                            />
                            <div className="banner__pic__slogan position-absolute fixed-top">
                                <h2 className="ms-4 mt-4 fw-bold">Smart Watch</h2>
                                <h4 className="ms-4 mt-1 fst-italic">New Collection - 2022</h4>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-6 col-md-6 col-12 my-2">
                        <div className="banner__pic position-relative">
                            <Image
                                src="/img/banner/banner-1.jpg"
                                width={450}
                                height={130}
                                layout="responsive"
                                quality={100}
                            />
                            <div className="banner__pic__slogan position-absolute fixed-top">
                                <h2 className="ms-4 mt-4 fw-bold fs-1-lg">B&O</h2>
                                <h4 className="ms-4 mt-1 fst-italic">Best Headphone Last year</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default BannerSection;

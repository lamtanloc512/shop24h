import React, { FC, useEffect, useState } from "react";
import Pagination from "../component/Pagination";
import ProductItem from "../component/ProductItem";
import { ProductInitialState } from "../reducer/ProductReducer";
import { ProductData } from "../utils/interface";

interface IDataFeature {
    payload?: ProductData[];
    pageNumber: number;
    totalPages: number;
    clickToChangePageNumber: (pageNumber: number) => void;
}
const FeaturedProductSection: FC<IDataFeature> = ({
    payload,
    totalPages,
    pageNumber,
    clickToChangePageNumber,
}) => {
    return (
        <section className="featured spad">
            <div className="container">
                <div className="row">
                    <div className="col-lg-12">
                        <div className="section-title">
                            <h2>Featured Product</h2>
                        </div>
                        <div className="featured__controls">
                            <ul>
                                <li className="active" data-filter="*">
                                    All
                                </li>
                                <li>iPhone</li>
                                <li>iPad</li>
                                <li>SmartWatch</li>
                                <li>Headphone</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="row featured__filter">
                    {payload != undefined ? (
                        payload.map((productElement: ProductData) => {
                            return (
                                <div
                                    key={productElement.id}
                                    className="col-6 col-lg-3 col-md-4 mix"
                                >
                                    <div className="featured__item">
                                        <ProductItem payload={productElement} />
                                    </div>
                                </div>
                            );
                        })
                    ) : (
                        <h2>No products founds!</h2>
                    )}
                </div>
                <Pagination
                    pageNumber={pageNumber}
                    totalPage={totalPages}
                    clickToChangePageNumber={clickToChangePageNumber}
                    position={"text-center"}
                />
            </div>
        </section>
    );
};

export default FeaturedProductSection;

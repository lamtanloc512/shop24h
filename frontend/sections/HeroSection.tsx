import React, { useEffect } from "react";
import Image from "next/image";
import CategoriesComponent from "../component/Home/CategoriesComponent";
import HeroSearchComponent from "../component/Home/HeroSearchComponent";
import HeroBannerComponent from "../component/Home/HeroBannerComponent";

const HeroSection = () => {
    return (
        <section className="hero">
            <div className="container">
                <div className="row">
                    <div className="col-lg-3">
                        <CategoriesComponent />
                    </div>
                    <div className="col-lg-9 position-relative">
                        <HeroSearchComponent />
                        <HeroBannerComponent />
                    </div>
                </div>
            </div>
        </section>
    );
};

export default HeroSection;

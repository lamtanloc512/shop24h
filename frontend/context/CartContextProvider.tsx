import React, { createContext, FC, useReducer, useEffect } from "react";
import { CartItem, cartReducer, cartStateType } from "./../reducer/ShoppingCartReducer";

const cartInitialState: cartStateType = {
    cartList: [],
};

export const CartContext: React.Context<any> = createContext(cartInitialState);

const CartContextProvider: FC = (props: any) => {
    // bỏ qua tham số thứ 2, dùng tham số thứ 3 và viết logic lấy data từ sessionStorage
    const [state, dispatch] = useReducer(cartReducer, {}, () => {
        let resultData: cartStateType = { cartList: [] };
        if (typeof window != "undefined") {
            let sessionData = sessionStorage.getItem("shop24h_cart");
            sessionData ? (resultData = JSON.parse(sessionData)) : (resultData = { cartList: [] });
        }
        return resultData;
    });

    useEffect(() => {
        sessionStorage.setItem("shop24h_cart", JSON.stringify(state));
    }, [state]);

    // add to cart action
    const addToCartAction = (payload: CartItem) => {
        dispatch({ type: "CHANGE_CART_AMOUNT", payload: payload });
    };

    // increase qty product in minicart
    const increaseQtyCart = (payload: CartItem) => {
        dispatch({ type: "INCREASE_CART_AMOUNT", payload: payload });
    };

    // decrease qty product in minicart
    const decreaseQtyCart = (payload: CartItem) => {
        dispatch({ type: "DECREASE_CART_AMOUNT", payload: payload });
    };
    // delete product in minicart
    const deleteProductCart = (payload: CartItem) => {
        dispatch({ type: "DELETE_CART_AMOUNT", payload: payload });
    };

    // change qty product in minicart
    const changeQtyProduct = (payload: CartItem) => {
        dispatch({ type: "ONCHANGE_CART_AMOUNT", payload: payload });
    };

    return (
        <CartContext.Provider
            value={{
                cartList: state.cartList,
                addToCartAction,
                increaseQtyCart,
                decreaseQtyCart,
                deleteProductCart,
                changeQtyProduct,
            }}
        >
            {props.children}
        </CartContext.Provider>
    );
};

export default CartContextProvider;

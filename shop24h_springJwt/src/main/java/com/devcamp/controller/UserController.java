package com.devcamp.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.devcamp.entity.RoleEntity;
import com.devcamp.entity.UserEntity;
import com.devcamp.service.UserService;

import lombok.Data;

@RestController
@CrossOrigin(value = "*", maxAge = -1)
@RequestMapping(value = "/api")
public class UserController {

	@Autowired
	private UserService userService;

	@GetMapping("/users")
	private ResponseEntity<List<UserEntity>> getUsers() {
		return new ResponseEntity<List<UserEntity>>(userService.getAllUser(), HttpStatus.OK);
	}

	@PostMapping("/user/save")
	private ResponseEntity<UserEntity> saveUser(@RequestBody UserEntity userEntity) {
		URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/user/save").toUriString());
		return ResponseEntity.created(uri).body(userService.saveUser(userEntity));
	}

	@PostMapping("/role/save")
	private ResponseEntity<RoleEntity> saveRole(@RequestBody RoleEntity roleEntity) {
		URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/role/save").toUriString());
		return ResponseEntity.created(uri).body(userService.saveRole(roleEntity));
	}

	@PostMapping("/role/addtouser")
	private ResponseEntity<?> addRoleToUser(@RequestBody RoleToUserForm form) {
		userService.addRoleToUser(form.getUsername(), form.getRolename());
		return ResponseEntity.ok().build();
	}

}

@Data
class RoleToUserForm {
	private String username;
	private String rolename;
}

package com.devcamp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.entity.Brands;
import com.devcamp.entity.Categories;
import com.devcamp.entity.ProductInfomation;
import com.devcamp.entity.Stock;
import com.devcamp.repository.BrandRepository;
import com.devcamp.repository.CategoryRepository;
import com.devcamp.repository.ProductInfoRepository;
import com.devcamp.repository.StockRepository;

@RestController
@CrossOrigin(value = "*", maxAge = -1)
public class TestController {

	@Autowired
	private ProductInfoRepository productInfoRepository;

	@Autowired
	private BrandRepository brandRepository;

	@Autowired
	private CategoryRepository categoryRepository;

	@Autowired
	private StockRepository stockRepository;

	@GetMapping("/product/info")
	public List<ProductInfomation> getProductInfo() {
		return productInfoRepository.findAll();
	}

	@GetMapping("/product/brands")
	public List<Brands> getBrands() {
		return brandRepository.findAll();
	}

	@GetMapping("/categories")
	public List<Categories> getCategories() {
		return categoryRepository.findAll();
	}

	@GetMapping("/product/stock")
	public List<Stock> getStock() {
		return stockRepository.findAll();
	}

}

package com.devcamp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.service.BrandService;
import com.devcamp.service.CategoryService;

@RestController
@CrossOrigin(value = "*", maxAge = -1)
@RequestMapping("/auth/admin")
public class AuthController {

	@Autowired
	private BrandService brandService;

	@Autowired
	private CategoryService categoryService;

	@GetMapping("/brands")
	private ResponseEntity<?> getAllBrands() {
		return ResponseEntity.ok().body(brandService.getBrands());
	}

	@GetMapping("/categories")
	private ResponseEntity<?> getAllCategories() {
		return ResponseEntity.ok().body(categoryService.getCategories());
	}

}

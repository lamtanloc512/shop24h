package com.devcamp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.devcamp.entity.Product;
import com.devcamp.service.ProductService;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@CrossOrigin(value = "*", maxAge = -1)
@RequestMapping("/product")
public class ProductController {

	@Autowired
	private ProductService productService;

	@GetMapping("/all")
	private List<Product> productList() {
		return productService.getAllProducts();
	}

	@GetMapping("/{id}")
	private ResponseEntity<?> getProductById(@PathVariable("id") Long id) {
		try {
			return ResponseEntity.ok().body(productService.getProductById(id));
		} catch (Exception e) {
			log.info("co loi xay ra o day: {}", e.getMessage());
			return ResponseEntity.notFound().build();
		}
	}

	@GetMapping("/pagination")
	private ResponseEntity<?> getProductWithPaging(@RequestParam(value = "index", defaultValue = "0") int index,
			@RequestParam(value = "per_page", defaultValue = "12") int perPage) {
		Pageable pageable = PageRequest.of(index, perPage);
		return ResponseEntity.ok().body(productService.getProductWithPaging(pageable));
	}

	@PostMapping("/create")
	private ResponseEntity<Object> createProduct(@RequestParam("product") String product,
			@RequestParam("img") MultipartFile imageFile) {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			Product productJsonToProduct = objectMapper.readValue(product, Product.class);
			return ResponseEntity.ok().body(productService.createProduct(productJsonToProduct, imageFile));
		} catch (Exception e) {
			return ResponseEntity.badRequest().body(e.getCause().getCause().getCause().getCause());
		}
	}

	@PutMapping("/update")
	private ResponseEntity<?> updateProduct(@RequestParam("product") String product,
			@RequestParam("img") MultipartFile imageFile) {

		try {
			ObjectMapper objectMapper = new ObjectMapper();
			Product productJsonToProduct = objectMapper.readValue(product, Product.class);
			return ResponseEntity.ok().body(productService.updateProduct(productJsonToProduct, imageFile));
		} catch (Exception e) {
			return ResponseEntity.badRequest().body(e.getCause().getCause().getCause().getCause());
		}
	}

	@DeleteMapping("/delete/{id}")
	private ResponseEntity<?> deleteProduct(@PathVariable("id") Long id) {

		return ResponseEntity.ok().body(productService.deleteProduct(id));
	}

}

package com.devcamp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.entity.Categories;
import com.devcamp.service.CategoryService;

@RestController
@CrossOrigin(value = "*", maxAge = -1)
@RequestMapping("/categories")
public class CategoriesController {

	@Autowired
	private CategoryService categoryService;

	@PostMapping("/create")
	private ResponseEntity<?> createCategory(@RequestBody Categories categories) {
		Categories _newCategories = categoryService.createNewCategory(categories);
		return new ResponseEntity<>(_newCategories, HttpStatus.OK);
	}

}

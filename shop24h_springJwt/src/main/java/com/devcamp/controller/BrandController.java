package com.devcamp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.entity.Brands;
import com.devcamp.service.BrandService;

@RestController
@CrossOrigin(value = "*", maxAge = -1)
@RequestMapping("/brand")
public class BrandController {

	@Autowired
	private BrandService brandService;

	@PostMapping("/create")
	private ResponseEntity<Object> createBrand(@RequestBody Brands brands) {
		Brands _newBrand = brandService.createNewBrand(brands);
		return new ResponseEntity<>(_newBrand, HttpStatus.OK);
	}

}

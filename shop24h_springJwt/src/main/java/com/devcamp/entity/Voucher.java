package com.devcamp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@Entity
@Table(name = "ps_voucher")
@AllArgsConstructor
@NoArgsConstructor
public class Voucher extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "voucher_name", nullable = true)
	private Long voucherName;

	@Column(name = "voucher_des", nullable = true)
	private Long voucherDes;

	@Column(name = "voucher_code", nullable = false)
	private Long voucherCode;

	@Column(name = "discount_percent", nullable = false)
	private Long discountPercent;

	@Column(name = "active", nullable = false)
	private boolean active;

}

package com.devcamp.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "ps_user")
@Data
public class UserEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", columnDefinition = "BINARY(16)")
	private UUID id;

	@Column(columnDefinition = "VARCHAR(255)")
	private String name;

	@Column(columnDefinition = "VARCHAR(255)")
	private String username;
	@Column(columnDefinition = "VARCHAR(3000)")
	private String password;

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "userEntity")
	private OrderDetail orderDetail;

	@ManyToMany(fetch = FetchType.EAGER)
	private Collection<RoleEntity> roles = new ArrayList<>();

	public UserEntity() {
	}

	/**
	 * @param id
	 * @param name
	 * @param username
	 * @param password
	 * @param orderDetail
	 * @param roles
	 */
	public UserEntity(UUID id, String name, String username, String password, OrderDetail orderDetail,
			Collection<RoleEntity> roles) {
		super();
		this.id = id;
		this.name = name;
		this.username = username;
		this.password = password;
		this.orderDetail = orderDetail;
		this.roles = roles;
	}

	/**
	 * @param id
	 * @param name
	 * @param username
	 * @param password
	 * @param roles
	 */
	public UserEntity(UUID id, String name, String username, String password, Collection<RoleEntity> roles) {
		super();
		this.id = id;
		this.name = name;
		this.username = username;
		this.password = password;
		this.roles = roles;
	}

}

package com.devcamp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "ps_role")
@Data
public class RoleEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "role")
	private String role;

	/**
	 * @param id
	 * @param role
	 */
	public RoleEntity(Long id, String role) {
		this.id = id;
		this.role = role;
	}

	/**
	 * 
	 */
	public RoleEntity() {
	}

}

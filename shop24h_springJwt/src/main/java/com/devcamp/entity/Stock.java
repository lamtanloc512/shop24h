package com.devcamp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "ps_product_stock")
public class Stock extends BaseEntity {
	private static final long serialVersionUID = 1L;

	@Column(name = "product_qty")
	private Long quantity;

	/**
	 * @param quantity
	 */
	public Stock(Long quantity) {
		super();
		this.quantity = quantity;
	}

	/**
	 * 
	 */
	public Stock() {
	}

}

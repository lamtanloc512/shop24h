package com.devcamp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "ps_product_info")
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ProductInfomation extends BaseEntity {

	private static final long serialVersionUID = 1L;

	@Column(name = "short_description", columnDefinition = "LONGTEXT")
	private String shortDescription;

	@Column(name = "long_description" , columnDefinition = "TEXT")
	private String longDescription;

	@Column(name = "additional_description", columnDefinition = "TEXT")
	private String additionalDescription;

}

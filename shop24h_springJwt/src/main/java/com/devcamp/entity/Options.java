package com.devcamp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "ps_product_options")
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Options extends BaseEntity {

	private static final long serialVersionUID = 1L;

	@Column(name = "option_name")
	private String optionName;

	@Column(name = "option_attr")
	private String optionAttribute;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "product_id", nullable = true)
	private Product product;

}

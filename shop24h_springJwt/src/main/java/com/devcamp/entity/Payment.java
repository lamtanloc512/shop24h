package com.devcamp.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ps_payment")
public class Payment extends BaseEntity {
	private static final long serialVersionUID = 1L;
	
	@Column(name = "amount", columnDefinition = "DECIMAL(10,2)")
	private Double amount;

	@Column(name = "status")
	private String status;

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "payment")
	private OrderDetail orderDetail;

}

package com.devcamp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "ps_gallery")
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ImageEntity extends BaseEntity {
	private static final long serialVersionUID = 1L;

	@Column(name = "img_alt", nullable = true)
	private String imgAlt;

	@Column(name = "img_permalink", nullable = false)
	private String imgPermalink;

	private String type;

	private String imgUri;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "product_id", nullable = true)
	private Product product;

	/**
	 * @param imgAlt
	 * @param imgPermalink
	 * @param type
	 * @param data
	 */
	public ImageEntity(String imgAlt, String imgPermalink, String type, String imgUri) {
		super();
		this.imgAlt = imgAlt;
		this.imgPermalink = imgPermalink;
		this.type = type;
		this.imgUri = imgUri;
	}

}

package com.devcamp.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
@Table(name = "ps_product")
public class Product extends BaseEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name = "product_name", nullable = true)
	private String productName;

	@Column(name = "product_slug", nullable = true)
	private String productSlug;

	@Column(name = "product_sku", nullable = true)
	private String productCode;

	@Column(name = "product_price", nullable = true)
	private String price;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "brand_id", nullable = true)
	private Brands brand;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "vendor_id", nullable = true)
	private Categories vendor;

	@OneToOne(cascade = CascadeType.ALL, targetEntity = Stock.class)
	@JoinColumn(name = "stock_id", nullable = true)
	private Stock stock;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, targetEntity = Voucher.class)
	@JoinColumn(name = "voucher_id", nullable = true)
	private Voucher voucher;

	@OneToOne(cascade = CascadeType.ALL, targetEntity = ProductInfomation.class)
	@JoinColumn(name = "product_info_id", nullable = true)
	private ProductInfomation productInfomation;

	@OneToOne(cascade = CascadeType.ALL, targetEntity = ImageEntity.class)
	@JoinColumn(name = "main_img_id", nullable = true)
	private ImageEntity productMainImg;

	@OneToMany(mappedBy = "product", cascade = CascadeType.ALL)
	private List<ImageEntity> gallery;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "product")
	private List<Options> productOpt = new ArrayList<>();

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "product")
	private OrderItem orderItem;

}

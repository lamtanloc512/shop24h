package com.devcamp.repository;

import com.devcamp.entity.Brands;

import org.springframework.data.jpa.repository.JpaRepository;

public interface BrandRepository extends JpaRepository<Brands, Long> {

}

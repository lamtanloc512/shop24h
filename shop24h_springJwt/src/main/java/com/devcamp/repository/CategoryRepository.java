package com.devcamp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.entity.Categories;

public interface CategoryRepository extends JpaRepository<Categories, Long> {

}

package com.devcamp.repository;

import com.devcamp.entity.ProductInfomation;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductInfoRepository extends JpaRepository<ProductInfomation, Long> {

}

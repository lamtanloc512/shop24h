package com.devcamp.repository;

import com.devcamp.entity.ImageEntity;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ImgRepository extends JpaRepository<ImageEntity, Long> {

}

package com.devcamp.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.entity.UserEntity;

public interface UserRepository extends JpaRepository<UserEntity, UUID>{
	UserEntity findByUsername(String username);
}

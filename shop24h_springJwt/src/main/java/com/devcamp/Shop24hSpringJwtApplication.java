package com.devcamp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
public class Shop24hSpringJwtApplication {

	public static void main(String[] args) {
		SpringApplication.run(Shop24hSpringJwtApplication.class, args);
	}

	@Bean
	BCryptPasswordEncoder bcryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}

//	@Bean
//	CommandLineRunner run(UserService userService, BrandService brandService ) {
//		return args -> {
//			// login
//			userService.saveRole(new RoleEntity(null, "ROLE_USER"));
//			userService.saveRole(new RoleEntity(null, "ROLE_MANAGER"));
//			userService.saveRole(new RoleEntity(null, "ROLE_ADMIN"));
//			userService.saveRole(new RoleEntity(null, "ROLE_SUPER_ADMIN"));
//			userService.saveUser(new UserEntity(null, "loclt", "lamtanloc512", "123456", new ArrayList<>()));
//			userService.saveUser(new UserEntity(null, "phatlt", "lamtanphat", "123456", new ArrayList<>()));
//			userService.saveUser(new UserEntity(null, "tienlt", "lamtantien", "123456", new ArrayList<>()));
//			userService.addRoleToUser("lamtanloc512", "ROLE_SUPER_ADMIN");
//			userService.addRoleToUser("lamtanloc512", "ROLE_ADMIN");
//			userService.addRoleToUser("lamtanloc512", "ROLE_MANAGER");
//			userService.addRoleToUser("lamtanloc512", "ROLE_USER");
//			userService.addRoleToUser("lamtanphat", "ROLE_USER");
//			userService.addRoleToUser("lamtanphat", "ROLE_MANAGER");
//			userService.addRoleToUser("lamtantien", "ROLE_USER");
//			
//			/// 
//			Brands _newBrands = new Brands();
//			_newBrands.setBrandName("Apple");
//			_newBrands.setBrandDescription("Apple Stuff");
//			Brands _newBrands2 = new Brands();
//			_newBrands2.setBrandName("Samsung");
//			_newBrands2.setBrandDescription("Samsung Stuff");
//			
//			brandService.createNewBrand(_newBrands);
//			brandService.createNewBrand(_newBrands2);
//		};
//	}

}

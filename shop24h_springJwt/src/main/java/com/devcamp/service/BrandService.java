package com.devcamp.service;

import java.util.List;

import com.devcamp.entity.Brands;

public interface BrandService {

	List<Brands> getBrands();

	Brands createNewBrand(Brands brands);

	String updateBrand(Brands brands);

	void deleteBrands(Long id);
}

package com.devcamp.service;

import java.util.List;

import com.devcamp.entity.Categories;

public interface CategoryService {

	List<Categories> getCategories();

	Categories createNewCategory(Categories categories);

	String updateCategory(Categories categories);

	void deleteCategory(Long id);
}

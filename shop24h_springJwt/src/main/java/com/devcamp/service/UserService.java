package com.devcamp.service;

import java.util.List;

import com.devcamp.entity.RoleEntity;
import com.devcamp.entity.UserEntity;

public interface UserService {
	UserEntity saveUser(UserEntity userEntity);

	RoleEntity saveRole(RoleEntity roleEntity);

	void addRoleToUser(String username, String roleName);

	UserEntity getUser(String username);

	List<UserEntity> getAllUser();
}

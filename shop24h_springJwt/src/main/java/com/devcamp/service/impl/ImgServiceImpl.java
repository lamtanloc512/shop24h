package com.devcamp.service.impl;

import java.io.File;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.devcamp.entity.ImageEntity;
import com.devcamp.repository.ImgRepository;
import com.devcamp.service.ImgService;

@Service
public class ImgServiceImpl implements ImgService {

	@Value("${file.upload-dir}")
	private String imgUploadPath;

	@Autowired
	private ImgRepository imgRepository;

	@Autowired
	private FileStorageService fileStorageService;

	@Override
	public ImageEntity storeImg(MultipartFile file) throws IOException {
		String fileName = fileStorageService.storeFile(file);
		String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath().path("/download/").path(fileName)
				.toUriString();

		File imagePermalink = new File(imgUploadPath + fileName);

		if (!imagePermalink.exists()) {
			file.transferTo(imagePermalink);
		}

		ImageEntity image = new ImageEntity(fileName, imagePermalink.toString(), file.getContentType(),
				fileDownloadUri);
		return imgRepository.save(image);
//		return null;
	}

}

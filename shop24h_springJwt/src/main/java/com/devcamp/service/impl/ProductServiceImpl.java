package com.devcamp.service.impl;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.devcamp.entity.Brands;
import com.devcamp.entity.Categories;
import com.devcamp.entity.ImageEntity;
import com.devcamp.entity.Product;
import com.devcamp.entity.Stock;
import com.devcamp.repository.BrandRepository;
import com.devcamp.repository.CategoryRepository;
import com.devcamp.repository.ProductRepository;
import com.devcamp.repository.StockRepository;
import com.devcamp.service.ImgService;
import com.devcamp.service.ProductService;

//@Slf4j
@Service
@Transactional
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private BrandRepository brandRepository;

	@Autowired
	private CategoryRepository categoryRepository;

	@Autowired
	private ImgService imgService;

	@Autowired
	private StockRepository stockRepository;

	@Override
	public Product createProduct(Product product, MultipartFile imageFile) throws IOException {
		Optional<Brands> _brandFound = Optional.empty();
		Optional<Categories> _categoryFound = Optional.empty();
		// tim trong csdl cac ban co san hay chua

		if (product.getBrand().getId() != null) {
			_brandFound = brandRepository.findById(product.getBrand().getId());
		}

		if (product.getVendor().getId() != null) {
			_categoryFound = categoryRepository.findById(product.getVendor().getId());
		}

		// save image va lay link save vao product
		ImageEntity _newImage = imgService.storeImg(imageFile);

		// tao moi entity
		Product _newProduct = new Product();
		_newProduct.setProductName(product.getProductName());
		_newProduct.setPrice(product.getPrice());
		_newProduct.setProductSlug(product.getProductName().toLowerCase().replace(" ", "-"));
		_newProduct.setProductCode(UUID.randomUUID().toString());
		_newProduct.setStock(new Stock(product.getStock().getQuantity()));
		_newProduct.setProductMainImg(_newImage);
		_newProduct.setProductInfomation(product.getProductInfomation());

		// neu trong csdl co brand thi set cho product brand
		if (_brandFound.isPresent()) {
			_newProduct.setBrand(_brandFound.get());
		}

		if (_categoryFound.isPresent()) {
			_newProduct.setVendor(_categoryFound.get());
		}

		else {
			// neu khong co brand thi tao moi cho product brand
			Brands _newBrand = new Brands();
			_newBrand.setBrandName(product.getBrand().getBrandName());
			_newBrand.setBrandDescription(product.getBrand().getBrandDescription());
			_newBrand.setBrandImgPath(product.getBrand().getBrandImgPath());
			_newProduct.setBrand(brandRepository.save(_newBrand));
		}

		return productRepository.save(_newProduct);

	}

	@Override
	public List<Product> getAllProducts() {
		return productRepository.findAll();
	}

	@Override
	public Product getProductById(Long id) {
		Optional<Product> _productDataFound = productRepository.findById(id);
		if (_productDataFound.isPresent()) {
			return _productDataFound.get();
		} else {
			return null;
		}
	}

	@Override
	public Page<Product> getProductWithPaging(Pageable pageable) {
		return productRepository.findAll(pageable);
	}

	@Override
	public String deleteProduct(Long id) {
		productRepository.deleteById(id);
		return new String("Product deleted!!!");
	}

	@Override
	public Product updateProduct(Product product, MultipartFile imageFile) throws IOException {
		Optional<Brands> _brandFound = Optional.empty();
		Optional<Categories> _categoryFound = Optional.empty();
		// tim entity trong csdl
		Optional<Product> _productDataFound = productRepository.findById(product.getId());

		if (_productDataFound.isPresent()) {
			// tim trong csdl cac brand, category co san hay chua
			if (product.getBrand().getId() != null) {
				_brandFound = brandRepository.findById(product.getBrand().getId());
			}

			if (product.getVendor().getId() != null) {
				_categoryFound = categoryRepository.findById(product.getVendor().getId());
			}

			Optional<Stock> _stockFound = stockRepository.findById(product.getStock().getId());
			Stock _updateStock = _stockFound.get();
			_updateStock.setQuantity(product.getStock().getQuantity());

			// save image va lay link save vao product
			ImageEntity _newImage = imgService.storeImg(imageFile);

			Product _productFound = _productDataFound.get();
			_productFound.setProductName(product.getProductName());
			_productFound.setPrice(product.getPrice());
			_productFound.setProductSlug(product.getProductName().toLowerCase().replace(" ", "-"));
			_productFound.setStock(_updateStock);
			_productFound.setProductMainImg(_newImage);
			_productFound.setProductInfomation(product.getProductInfomation());

			// neu trong csdl co brand thi set cho product brand
			if (_brandFound.isPresent()) {
				_productFound.setBrand(_brandFound.get());
			}

			if (_categoryFound.isPresent()) {
				_productFound.setVendor(_categoryFound.get());
			}

			return productRepository.save(_productFound);

		} else {

			return null;
		}

	}

}

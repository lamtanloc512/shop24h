package com.devcamp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.entity.Categories;
import com.devcamp.repository.CategoryRepository;
import com.devcamp.service.CategoryService;

@Service
public class CategoryServiceImpl implements CategoryService {

	@Autowired
	private CategoryRepository categoryRepository;

	@Override
	public List<Categories> getCategories() {
		return categoryRepository.findAll();
	}

	@Override
	public Categories createNewCategory(Categories categories) {
		Categories _newCategories = new Categories();
		_newCategories.setVendorName(categories.getVendorName());
		_newCategories.setVendorDescription(categories.getVendorDescription());
		return categoryRepository.save(_newCategories);
	}

	@Override
	public String updateCategory(Categories categories) {
		return null;
	}

	@Override
	public void deleteCategory(Long id) {

	}

}

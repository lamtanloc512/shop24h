package com.devcamp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.entity.Brands;
import com.devcamp.repository.BrandRepository;
import com.devcamp.service.BrandService;

@Service
public class BrandServiceImpl implements BrandService {

	@Autowired
	private BrandRepository brandRepository;

	@Override
	public List<Brands> getBrands() {
		return brandRepository.findAll();
	}

	@Override
	public Brands createNewBrand(Brands brands) {
		Brands _newBrands = new Brands();
		_newBrands.setBrandName(brands.getBrandName());
		_newBrands.setBrandDescription(brands.getBrandDescription());

		return brandRepository.save(_newBrands);
	}

	@Override
	public String updateBrand(Brands brands) {
		return null;
	}

	@Override
	public void deleteBrands(Long id) {

	}

}

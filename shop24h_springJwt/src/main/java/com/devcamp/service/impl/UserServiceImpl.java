package com.devcamp.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.devcamp.entity.RoleEntity;
import com.devcamp.entity.UserEntity;
import com.devcamp.repository.RoleRepository;
import com.devcamp.repository.UserRepository;
import com.devcamp.security.UserPrincipal;
import com.devcamp.service.UserService;

import lombok.extern.slf4j.Slf4j;

@Service
@Transactional
@Slf4j
public class UserServiceImpl implements UserService, UserDetailsService {

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserEntity userEntity = userRepository.findByUsername(username);
		if (userEntity == null) {
			log.error("user not found in db");
			throw new UsernameNotFoundException("Username not found");
		} else {
			log.info("user found in db : {}", username);
		}

		Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
		userEntity.getRoles().forEach(role -> authorities.add(new SimpleGrantedAuthority(role.getRole())));

		return new UserPrincipal(userEntity.getUsername(), userEntity.getPassword(), authorities);
	}

	@Override
	public UserEntity saveUser(UserEntity userEntity) {
		userEntity.setPassword(bCryptPasswordEncoder.encode(userEntity.getPassword()));
		return userRepository.save(userEntity);
	}

	@Override
	public RoleEntity saveRole(RoleEntity roleEntity) {
		return roleRepository.save(roleEntity);
	}

	@Override
	public void addRoleToUser(String username, String roleName) {
		UserEntity userEntity = userRepository.findByUsername(username);
		RoleEntity roleEntity = roleRepository.findByRole(roleName);
		userEntity.getRoles().add(roleEntity);

	}

	@Override
	public UserEntity getUser(String username) {
		return userRepository.findByUsername(username);
	}

	@Override
	public List<UserEntity> getAllUser() {
		List<UserEntity> _listUser = new ArrayList<>();
		userRepository.findAll().forEach(_listUser::add);
		return _listUser;
	}

}

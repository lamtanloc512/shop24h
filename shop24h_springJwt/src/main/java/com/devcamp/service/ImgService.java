package com.devcamp.service;

import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

import com.devcamp.entity.ImageEntity;

public interface ImgService {
	ImageEntity storeImg(MultipartFile file) throws IOException;
}

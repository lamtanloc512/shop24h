package com.devcamp.service;

import java.io.IOException;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import com.devcamp.entity.Product;

public interface ProductService {
	Product createProduct(Product product, MultipartFile imageFile) throws IOException;

	List<Product> getAllProducts();

	Product getProductById(Long id);

	Page<Product> getProductWithPaging(Pageable pageable);

	String deleteProduct(Long id);

	Product updateProduct(Product product, MultipartFile imageFile) throws IOException;
}

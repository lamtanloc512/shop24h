export const ProductAction = {
    GET_PRODUCTS: "GET_PRODUCTS",
    RESET_LOADING: "RESET_LOADING",
    CREATE_PRODUCT_SUCCESS: "CREATE_PRODUCT_SUCCESS",
    CREATE_PRODUCT_LOADING: "CREATE_PRODUCT_LOADING",
    CREATE_PRODUCT_ERROR: "CREATE_PRODUCT_ERROR",
    UPDATE_PRODUCT: "UPDATE_PRODUCT",
    DELETE_PRODUCT_SUCCESS: "DELETE_PRODUCT_SUCCESS",
    DELETE_PRODUCT_LOADING: "DELETE_PRODUCT_LOADING",
    DELETE_PRODUCT_ERROR: "DELETE_PRODUCT_ERROR",
};

export const ProductReducer = (state, { type, payload }) => {
    switch (type) {
        case ProductAction.GET_PRODUCTS:
            return { ...state, products: payload };

        case ProductAction.CREATE_PRODUCT_SUCCESS:
            return { ...state, payload: payload, status: { loading: false, success: true } };

        case ProductAction.CREATE_PRODUCT_LOADING:
            return { ...state, status: { loading: true, success: false } };

        case ProductAction.CREATE_PRODUCT_ERROR:
            return { ...state, status: { loading: false, success: false } };

        case ProductAction.RESET_LOADING:
            return { state, status: { loading: false, success: null } };

        case ProductAction.UPDATE_PRODUCT:
            console.log("update product");
            break;

        case ProductAction.DELETE_PRODUCT_SUCCESS:
            console.log("delete product");
            return { state, status: { loading: false, success: true } };

        case ProductAction.DELETE_PRODUCT_ERROR:
            console.log("delete product");
            return { state, status: { loading: false, success: false } };

        default:
            return state;
    }
};

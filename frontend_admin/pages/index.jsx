import Head from "next/head";
import { Fragment } from "react";

export default function Home() {
    return (
        <Fragment>
            <Head>
                <title>Proshop24h</title>
                <meta name="description" content="Proshop24h Admin" />
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <div style={{ height: "500px" }}>this is Home page</div>
        </Fragment>
    );
}

import React, { useEffect, useState, useContext } from "react";
import $ from "jquery";
import Script from "next/script";
import Head from "next/head";
import { Row, Col, Container } from "react-bootstrap";
import Select from "react-select";
import ImageProductUpload from "../../../components/elements/ImageProductUpload";
import { ProductContext } from "../../../context/ProductContext";
import Link from "next/link";
import CreateBrandModal from "../../../components/product/CreateBrandModal";
import CreateCategoryModal from "../../../components/product/CreateCategoryModal";
import { useRouter } from "next/router";
import { LOCAL_PRODUCT_URL } from "../../../constant/developmentURL";

const DetailProduct = ({ brands, categories }) => {
    const [modalCreateBrandShow, setModalCreateBrandShow] = useState(false);
    const [modalCreateCategoryShow, setModalCreateCategoryShow] = useState(false);
    const [productTitle, setProductTitle] = useState("");
    const [productShortDescription, setProductShortDescription] = useState("");
    const [productLongDescription, setProductLongDescription] = useState("");
    const [productAddDescription, setProductAddDescription] = useState("");
    const [productRegularPrice, setProductRegularPrice] = useState("");
    const [productQuantity, setProductQuantity] = useState("");
    const [productBrand, setProductBrand] = useState(null);
    const [productCategory, setProductCategory] = useState(null);
    const [resetPreview, setResetPreview] = useState(false);
    const [optionsBrands, setOptionsBrands] = useState([]);
    const [optionsCategories, setOptionsCategories] = useState([]);
    const [imgUrl, setImgUrl] = useState(null);

    const router = useRouter();
    const { id } = router.query;

    let gResult = null;
    // đổ dữ liệu vào cac field
    useEffect(() => {
        const getProductDetailFromApi = async () => {
            if (gResult == null) {
                const vRequest = await fetch(`${LOCAL_PRODUCT_URL + id}`, {
                    method: "GET",
                });

                if (vRequest.ok) {
                    const vResponse = await vRequest.json();
                    gResult = vResponse;
                    // console.log(vResponse);
                }
            }

            if (gResult != null) {
                setProductTitle(gResult.productName);
                setProductShortDescription(gResult.productInfomation.shortDescription);
                setProductLongDescription(gResult.productInfomation.longDescription);
                setProductAddDescription(gResult.productInfomation.additionalDescription);
                setProductRegularPrice(gResult.price);
                setProductQuantity(gResult.stock.quantity);
                setProductBrand(
                    gResult.brand != null
                        ? {
                              value: gResult.brand.id,
                              label: gResult.brand.brandName,
                          }
                        : null,
                );
                setProductCategory(
                    gResult.vendor != null
                        ? {
                              value: gResult.vendor.id,
                              label: gResult.vendor.vendorName,
                          }
                        : null,
                );

                setImgUrl(gResult.productMainImg.imgPermalink);
            }
        };

        if (categories != null) {
            let vOptCatgories = [];
            categories.forEach((categoryElement) => {
                vOptCatgories.push({
                    value: categoryElement.id,
                    label: categoryElement.vendorName,
                });
                setOptionsCategories(() => vOptCatgories);
            });
        }

        if (brands != null) {
            let vOptBrands = [];
            brands.forEach((brandElement) => {
                vOptBrands.push({
                    value: brandElement.id,
                    label: brandElement.brandName,
                });
                setOptionsBrands(() => vOptBrands);
            });
        }

        getProductDetailFromApi();
    }, [gResult]);

    // context api product
    const { status, resetLoading, createNewProduct } = useContext(ProductContext);

    const handleSubmit = () => {
        const vProduct = {
            productName: productTitle,
            price: productRegularPrice,
            brand:
                productBrand != null
                    ? {
                          id: productBrand.value,
                      }
                    : {},
            vendor:
                productCategory != null
                    ? {
                          id: productCategory.value,
                      }
                    : {},
            stock: {
                quantity: parseInt(productQuantity),
            },
            productInfomation: {
                shortDescription: productShortDescription,
                longDescription: productLongDescription,
                additionalDescription: productAddDescription,
            },
        };
        const vImgFile = $("#product__main__img").prop("files")[0];
        createNewProduct({
            product: vProduct,
            img: vImgFile,
            status: {
                loading: true,
                success: false,
            },
        });
    };

    const handleCancel = () => {
        setProductTitle("");
        setProductShortDescription("");
        setProductLongDescription("");
        setProductAddDescription("");
        setProductRegularPrice("");
        setProductQuantity("");
        setProductBrand(null);
        setProductCategory(null);
    };

    const handleCreateBrand = () => {
        const newBrand = {
            brandName: $("#brand__name").val(),
            brandDescription: $("#brand__des").val(),
        };

        fetch("http://localhost:9000/brand/create", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(newBrand),
        })
            .then((response) => response.json())
            .then((result) => {
                console.log(result);
                toastr.success("Brands created successfully!!!");
                setOptionsBrands([
                    ...optionsBrands,
                    {
                        value: result.id,
                        label: result.brandName,
                    },
                ]);
            })
            .catch((error) => {
                console.log("error", error);
                toastr.error("Create failed !!!");
            });
    };

    const handleCreateCategory = () => {
        const newCategory = {
            vendorName: $("#category__name").val(),
            vendorDescription: $("#category__des").val(),
        };

        fetch("http://localhost:9000/categories/create", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(newCategory),
        })
            .then((response) => response.json())
            .then((result) => {
                console.log(result);
                toastr.success("Category created successfully!!!");
                setOptionsCategories([
                    ...optionsCategories,
                    {
                        value: result.id,
                        label: result.vendorName,
                    },
                ]);
            })
            .catch((error) => {
                console.log("error", error);
                toastr.error("Create failed !!!");
            });
    };

    useEffect(() => {
        if (status.success) {
            toastr.success("Create product successfully!!!");
            resetLoading();
            handleCancel();
            setResetPreview(true);
        } else if (!status.success && status.success != null) {
            toastr.error("Failed Create product !!!");
        }
    }, [status.success]);

    return (
        <>
            <Head>
                <title>Product detail</title>
            </Head>
            <Container fluid>
                {/* <p>{JSON.stringify(id)}</p> */}
                <div className="content-header">
                    <div className="container-fluid">
                        <div className="row mb-2">
                            <div className="col-sm-6">
                                <h1 className="m-0">Product detail</h1>
                            </div>
                            <div className="col-sm-6">
                                <ol className="breadcrumb float-sm-right">
                                    <li className="breadcrumb-item">
                                        <Link href="/">
                                            <a style={{ cursor: "pointer" }}>Home</a>
                                        </Link>
                                    </li>
                                    <li className="breadcrumb-item active">Product detail</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <Row>
                    <Col md={9}>
                        <div className="card">
                            <div className="card-header">
                                <Link href="/products">
                                    <button className="btn btn-warning text-bold">
                                        <i className="fa fa-arrow-left mx-2" aria-hidden="true" />{" "}
                                        Go back
                                    </button>
                                </Link>
                            </div>
                            <div className="card-body">
                                <div className="form-group">
                                    <input
                                        className="form-control"
                                        placeholder="Product title"
                                        onChange={(e) => setProductTitle(e.target.value)}
                                        value={productTitle}
                                    />
                                </div>
                                <Row>
                                    <Col>
                                        <div className="form-group">
                                            <textarea
                                                id="textarea__short_description"
                                                className="form-control"
                                                style={{ height: "100px" }}
                                                onChange={(e) =>
                                                    setProductShortDescription(e.target.value)
                                                }
                                                value={productShortDescription}
                                                placeholder="Product short description"
                                            />
                                        </div>
                                    </Col>
                                    <Col>
                                        <div className="form-group">
                                            <textarea
                                                id="textarea__long_description"
                                                className="form-control"
                                                style={{ height: "100px" }}
                                                onChange={(e) =>
                                                    setProductLongDescription(e.target.value)
                                                }
                                                value={productLongDescription}
                                                placeholder="Product long description"
                                            />
                                        </div>
                                    </Col>
                                    <Col className="col-12">
                                        <div className="form-group">
                                            <textarea
                                                id="textarea__additional_description"
                                                className="form-control"
                                                style={{ height: "250px" }}
                                                onChange={(e) =>
                                                    setProductAddDescription(e.target.value)
                                                }
                                                value={productAddDescription}
                                                placeholder="Product additional description"
                                            />
                                        </div>
                                    </Col>
                                </Row>

                                <Row>
                                    <Col>
                                        <div className="form-group">
                                            <input
                                                type={"text"}
                                                className="form-control"
                                                placeholder="Product price"
                                                onChange={(e) =>
                                                    setProductRegularPrice(e.target.value)
                                                }
                                                value={productRegularPrice}
                                            />
                                        </div>
                                    </Col>
                                    <Col>
                                        <div className="form-group">
                                            <input
                                                type={"text"}
                                                className="form-control"
                                                placeholder="Product quantity"
                                                onChange={(e) => setProductQuantity(e.target.value)}
                                                value={productQuantity}
                                            />
                                        </div>
                                    </Col>
                                    <Col>
                                        <div className="form-group">
                                            <Select
                                                id="select__product_brand"
                                                instanceId="select__product_category"
                                                options={optionsBrands}
                                                name="category"
                                                className="select__product__category"
                                                classNamePrefix="shop24h"
                                                onChange={(value) => setProductBrand(value)}
                                                isClearable
                                                value={productBrand}
                                            />
                                        </div>
                                        <button
                                            className="btn btn-success text-bold m-1 float-right"
                                            onClick={() => setModalCreateBrandShow(true)}
                                        >
                                            <i className="fa fa-plus mr-2" aria-hidden="true" />{" "}
                                            Create Brand
                                        </button>
                                    </Col>
                                </Row>
                            </div>
                        </div>
                        <div className="card">
                            <div className="card-header">Product gallery</div>
                            <div className="card-body">
                                <button className="btn btn-success text-bold m-1 float-left">
                                    <i className="fa fa-plus mr-2" aria-hidden="true" /> Create
                                    gallery
                                </button>
                            </div>
                        </div>
                    </Col>
                    <Col md={3}>
                        <div className="card">
                            <div className="card-header">Publish</div>
                            <div className="card-body d-flex justify-content-end">
                                <button
                                    className="btn btn-primary text-bold m-1"
                                    onClick={handleSubmit}
                                >
                                    Save
                                </button>
                                <button
                                    className="btn btn-danger text-bold m-1"
                                    onClick={handleCancel}
                                >
                                    Cancel
                                </button>
                            </div>
                        </div>
                        <div className="card">
                            <div className="card-header">Categories</div>
                            <div className="card-body">
                                <div className="form-group">
                                    <Select
                                        id="select__product_category"
                                        instanceId="select__product_category"
                                        options={optionsCategories}
                                        name="category"
                                        className="select__product__category"
                                        classNamePrefix="shop24h"
                                        onChange={(value) => setProductCategory(value)}
                                        isClearable
                                        value={productCategory}
                                    />
                                </div>
                                <button
                                    className="btn btn-success text-bold m-1 float-right"
                                    onClick={() => setModalCreateCategoryShow(true)}
                                >
                                    <i className="fa fa-plus mr-2" aria-hidden="true" /> Create
                                    category
                                </button>
                            </div>
                        </div>
                        <div className="card">
                            <div className="card-header">Product Image</div>
                            <div className="card-body">
                                <ImageProductUpload
                                    id={"product__main__img"}
                                    resetPreview={resetPreview}
                                    imgUrl={imgUrl}
                                />
                            </div>
                        </div>
                    </Col>
                </Row>
                <CreateBrandModal
                    show={modalCreateBrandShow}
                    onHide={() => setModalCreateBrandShow(false)}
                    handleCreateBrand={handleCreateBrand}
                />
                <CreateCategoryModal
                    show={modalCreateCategoryShow}
                    onHide={() => setModalCreateCategoryShow(false)}
                    handleCreateCategory={handleCreateCategory}
                />
            </Container>
            <Script src="/assets/custom.js" strategy="lazyOnload" />
        </>
    );
};

export default DetailProduct;

export async function getStaticPaths() {
    let myHeaders = new Headers();
    myHeaders.append("Authorization", "Bearer ");

    let requestOptions = {
        method: "GET",
        headers: myHeaders,
        redirect: "follow",
    };

    const req = await fetch("http://localhost:9000/product/all", requestOptions);
    const res = await req.json();

    let paths = [];

    for (let index = 0; index < res.length; index++) {
        paths.push({ params: { id: res[index].id.toString() } });
    }

    return {
        paths,
        fallback: false,
    };
}

export const getStaticProps = async () => {
    let vResultBrands = null;
    let vResultCategories = null;
    // goi api lay du lieu brand
    try {
        const vReqBrands = await fetch(`http://localhost:9000/auth/admin/brands`, {
            method: "GET",
        });

        const vReqCategories = await fetch(`http://localhost:9000/auth/admin/categories`, {
            method: "GET",
        });

        // catch exception, return empty array
        if (!vReqBrands.ok) {
            vResultBrands = {
                response: vReqBrands.statusText,
                status: vReqBrands.status,
            };
        }
        if (!vReqCategories.ok) {
            vResultCategories = {
                response: vReqCategories.statusText,
                status: vReqCategories.status,
            };
        }

        vResultBrands = await vReqBrands.json();
        vResultCategories = await vReqCategories.json();
    } catch (error) {
        console.log(error.errno);
        // console.log(vResultBrands);
    }

    return {
        props: {
            brands: vResultBrands,
            categories: vResultCategories,
        },
    };
};

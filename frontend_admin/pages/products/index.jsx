import React, { Fragment, useState, useEffect, useContext } from "react";
import Head from "next/head";
import Link from "next/link";
import { Container, Row, Col } from "react-bootstrap";
import DatatableComponent from "../../components/product/DatatableComponent";
import DeleteProductModal from "../../components/product/DeleteProductModal";
import $ from "jquery";
import { ProductContext } from "../../context/ProductContext";
import { useRouter } from "next/router";

const Products = () => {
    const [modalDeleteProductShow, setModalDeleteProductShow] = useState(false);
    const [productId, setProductId] = useState(null);
    const [reloadTable, setReloadTable] = useState(false);
    const { products, status, getAllProducts, deleteProduct, resetLoading } =
        useContext(ProductContext);
    const [productDataToTable, setProductDataToTable] = useState(null);

    const router = useRouter();

    useEffect(() => {
        // call event delete button delete in conponent DeleteProductModal
        $(".btn__delete__product").on("click", (e) => {
            setModalDeleteProductShow(true);
            setProductId(e.target.dataset.id);
        });
        $(".btn__edit__product").on("click", (e) => {
            let productId = e.target.dataset.id;
            router.push(`/products/detail/${productId}`);
        });
        return () => {
            $(".btn__delete__product").off();
            $(".btn__edit__product").off();
        };
    }, [reloadTable]);

    useEffect(() => {
        if (products == null) {
            getAllProducts();
        }

        if (products != null) {
            setProductDataToTable(products);
            setReloadTable(!reloadTable);
        }
    }, [products]);

    useEffect(() => {
        if (status.success) {
            toastr.success("Delete product successfully!!!");
        } else if (!status.success && status.success != null) {
            toastr.error("Failed Delete product !!!");
        }
    }, [status.success]);

    const handleDelete = (id) => {
        deleteProduct(id);
        getAllProducts();
    };

    return (
        <Fragment>
            <Head>
                <title>Products</title>
            </Head>
            <Container fluid>
                {/* <p>{JSON.stringify(status)}</p> */}
                <div className="content-header">
                    <div className="container-fluid">
                        <div className="row mb-2">
                            <div className="col-sm-6">
                                <h1 className="m-0">Products</h1>
                            </div>
                            <div className="col-sm-6">
                                <ol className="breadcrumb float-sm-right">
                                    <li className="breadcrumb-item">
                                        <Link href="/">
                                            <a style={{ cursor: "pointer" }}>Home</a>
                                        </Link>
                                    </li>
                                    <li className="breadcrumb-item active">Products</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <Row>
                    <Col>
                        <div className="card w-100">
                            <div className="card-header">
                                <Link href="/products/create">
                                    <button className="btn btn-success text-bold">
                                        <i className="fa fa-plus mx-2" aria-hidden="true" /> Create
                                        new
                                    </button>
                                </Link>
                            </div>
                            <div className="card-body">
                                <DatatableComponent
                                    data={productDataToTable != null ? productDataToTable : null}
                                    reloadTable={reloadTable}
                                />
                            </div>
                        </div>
                    </Col>
                </Row>
            </Container>
            <DeleteProductModal
                show={modalDeleteProductShow}
                onHide={() => setModalDeleteProductShow(false)}
                id={productId}
                handleDelete={handleDelete}
            />
        </Fragment>
    );
};
export default Products;

// export const getStaticProps = async () => {
// let myHeaders = new Headers();
// myHeaders.append("Authorization", "Bearer ");

// let requestOptions = {
//     method: "GET",
//     headers: myHeaders,
//     redirect: "follow",
// };

// const req = await fetch("http://localhost:9000/product/all", requestOptions);
// const res = await req.json();
// console.log(res);

//     return {
//         props: {
//             data: res,
//         },
//     };
// };

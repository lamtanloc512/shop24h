import Layout from "../components/Layout";
// import "bootstrap/dist/css/bootstrap.min.css";
import "../public/dist/css/adminlte.css";
import "../styles/globals.css";
import ProductContextProvider from "../context/ProductContext";
import { useRouter } from "next/router";
import NextNProgress from "nextjs-progressbar";

function MyApp({ Component, pageProps }) {
    const router = useRouter();
    const SwitchComponent = () => {
        if (router.asPath.includes("products")) {
            return (
                <ProductContextProvider>
                    <Layout>
                        <NextNProgress />
                        <Component {...pageProps} />
                    </Layout>
                </ProductContextProvider>
            );
        } else {
            return (
                <Layout>
                    <NextNProgress />
                    <Component {...pageProps} />
                </Layout>
            );
        }
    };

    return <>{SwitchComponent()}</>;
}

export default MyApp;

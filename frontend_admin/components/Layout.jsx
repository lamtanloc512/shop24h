import React, { Fragment } from "react";
import Navbar from "./elements/Navbar";
import Sidebar from "./elements/Sidebar";
import Footer from "./elements/Footer";
import ControlSidebar from "./elements/ControlSidebar";
import Script from "next/script";
import Head from "next/head";

const Layout = ({ children }) => {
    return (
        <Fragment>
            <Head>
                <link rel="stylesheet" href="/plugins/fontawesome-free/css/all.min.css" />
                {/* <link rel="stylesheet" href="/plugins/dropzone/min/dropzone.min.css" /> */}
                {/* <link rel="stylesheet" href="/plugins/summernote/summernote-bs4.min.css" /> */}
                <link rel="stylesheet" href="/plugins/toastr/toastr.min.css" />
                <link
                    rel="stylesheet"
                    href="/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css"
                />
                <link
                    rel="stylesheet"
                    href="/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css"
                />
                {/* <link
                    rel="stylesheet"
                    href="/plugins/datatables-responsive/css/responsive.bootstrap4.min.css"
                /> */}
                {/* <link
                    rel="stylesheet"
                    href="/plugins/datatables-buttons/css/buttons.bootstrap4.min.css"
                /> */}
                {/* <link rel="stylesheet" href="/dist/css/adminlte.min.css" /> */}
            </Head>
            <Navbar />
            <Sidebar />
            <div className="content-wrapper h-100">{children}</div>
            <ControlSidebar />
            <Footer />
            <Script src="/plugins/jquery/jquery.min.js" strategy="beforeInteractive" />
            <Script
                src="/plugins/bootstrap/js/bootstrap.bundle.min.js"
                strategy="beforeInteractive"
            />
            <Script src="/plugins/toastr/toastr.min.js" strategy="beforeInteractive" />
            <Script src="/dist/js/adminlte.min.js" />
        </Fragment>
    );
};

export default Layout;

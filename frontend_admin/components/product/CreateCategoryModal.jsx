import React from "react";
import { Modal, Button } from "react-bootstrap";

const CreateCategoryModal = ({ show, onHide, handleCreateCategory }) => {
    return (
        <Modal
            show={show}
            onHide={onHide}
            size="md"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header>
                <Modal.Title id="contained-modal-title-vcenter">Create new category</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <div className="form-group">
                    <label>Category name</label>
                    <input type="text" className="form-control" id="category__name" />
                </div>
                <div className="form-group">
                    <label>Category description</label>
                    <textarea className="form-control" rows="3" id="category__des" />
                </div>
            </Modal.Body>
            <Modal.Footer>
                <Button className="text-bold" variant="success" onClick={handleCreateCategory}>
                    Save
                </Button>
                <Button className="text-bold" variant="danger" onClick={onHide}>
                    Close
                </Button>
            </Modal.Footer>
        </Modal>
    );
};

export default CreateCategoryModal;

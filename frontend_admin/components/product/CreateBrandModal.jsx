import React from "react";
import { Modal, Button } from "react-bootstrap";

const CreateBrandModal = ({ show, onHide, handleCreateBrand }) => {
    return (
        <Modal
            show={show}
            onHide={onHide}
            size="md"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header>
                <Modal.Title id="contained-modal-title-vcenter">Create brand</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <div className="form-group">
                    <label>Brand name</label>
                    <input type="text" className="form-control" id="brand__name" />
                </div>
                <div className="form-group">
                    <label>Brand description</label>
                    <textarea className="form-control" rows="3" id="brand__des" />
                </div>
            </Modal.Body>
            <Modal.Footer>
                <Button className="text-bold" variant="success" onClick={handleCreateBrand}>
                    Save
                </Button>
                <Button className="text-bold" variant="danger" onClick={onHide}>
                    Close
                </Button>
            </Modal.Footer>
        </Modal>
    );
};

export default CreateBrandModal;

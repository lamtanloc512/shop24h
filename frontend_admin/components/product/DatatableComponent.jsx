import $ from "jquery";
import { Component } from "react";
import JSZip from "jszip";
import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
import "datatables.net-bs4";
import "datatables.net-responsive-bs4";
import "datatables.net-buttons-bs4";
import "datatables.net-buttons/js/buttons.colVis";
import "datatables.net-buttons/js/buttons.html5";
import "datatables.net-buttons/js/buttons.flash";
import "datatables.net-buttons/js/buttons.print";

pdfMake.vfs = pdfFonts.pdfMake.vfs;

function loadTable(paramData) {
    if (typeof window !== "undefined") {
        window.JSZip = JSZip;
    }

    const gColumn = ["Img", "Sku", "Name", "Price", "Brand", "Vendor", "Stock", "ID"];

    let vData = [];

    if (paramData != null) {
        paramData.map((bI) => {
            let vNewElement = {
                Img: bI.productMainImg.imgPermalink.replace(
                    "C:\\Users\\Admin\\Desktop\\imgStore\\",
                    "http://127.0.0.1:8080/",
                ),
                Sku: bI.productCode,
                Name: bI.productName,
                Price: bI.price,
                Brand: bI.brand != null ? bI.brand.brandName : "",
                Vendor: bI.vendor != null ? bI.vendor.vendorName : "",
                Stock: bI.stock.quantity,
                ID: bI.id,
            };
            vData.push(vNewElement);
        });
    }

    let vDataTbl = $("#product__datatable")
        .DataTable({
            retrieve: true,
            responsive: true,
            lengthChange: false,
            autoWidth: false,
            data: vData,
            // dom: "Bfrtip",
            buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
            columns: [
                {
                    data: gColumn[0],
                    render: function (data) {
                        return `
                              <div class="tbl__img__wrapper">
                                <img class="img-fluid" src="${data}"/>
                              </div>
                            `;
                    },
                },
                { data: gColumn[1] },
                { data: gColumn[2] },
                {
                    data: gColumn[3],
                    render: function (data) {
                        return `
                            <div class="d-flex">
                                <h6 class="mx-1">$</h6> <strong> ${data}</strong>
                            </div>
                            `;
                    },
                },
                { data: gColumn[4] },
                { data: gColumn[5] },
                { data: gColumn[6] },
                {
                    data: gColumn[7],
                    render: function (data) {
                        return `
                              <div class="btn-group d-flex justify-content-center" role="group">
                                <button class="btn__edit__product btn-edit btn btn-sm btn-primary text-bold m-0 w-100" data-id=${data}>Edit</button>
                                <button class="btn__delete__product btn btn-sm btn-danger text-bold m-0 w-100" data-id=${data}>Delete</button>
                              </div>
                            `;
                    },
                },
            ],
        })
        .buttons()
        .container()
        .appendTo("#product__datatable_wrapper .col-md-6:eq(0)");
}

export default class DatatableComponent extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        loadTable(this.props.data);
    }

    componentDidUpdate() {
        loadTable(this.props.data);
    }

    componentWillUnmount() {
        $("#product__datatable").DataTable().destroy();
    }

    render() {
        return (
            <div key={this.props.reloadTable}>
                <table id="product__datatable" className="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Img</th>
                            <th>Sku</th>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Brand</th>
                            <th>Vendor</th>
                            <th>Stock</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                    <tfoot>
                        <tr>
                            <th>Img</th>
                            <th>Sku</th>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Brand</th>
                            <th>Vendor</th>
                            <th>Stock</th>
                            <th>Actions</th>
                        </tr>
                    </tfoot>
                </table>
                {JSON.stringify(this.props.test)}
            </div>
        );
    }
}

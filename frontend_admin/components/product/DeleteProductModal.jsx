import React from "react";
import { Modal, Button, Row, Col } from "react-bootstrap";

const DeleteProductModal = ({ show, onHide, handleDelete, id }) => {
    return (
        <Modal
            show={show}
            onHide={onHide}
            size="md"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header>
                <Modal.Title id="contained-modal-title-vcenter">Delete Product</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Row>
                    <Col className="text-center">
                        <h3>Are you sure to delete this product ?</h3>
                    </Col>
                </Row>
            </Modal.Body>
            <Modal.Footer>
                <Button className="text-bold" variant="danger" onClick={() => handleDelete(id)}>
                    Confirm
                </Button>
                <Button className="text-bold" variant="dark" onClick={onHide}>
                    Close
                </Button>
            </Modal.Footer>
        </Modal>
    );
};

export default DeleteProductModal;

import React from "react";

const Footer = () => {
    return (
        // <footer className="main-footer" style={{ position: "fixed", bottom: "0", width: "100%" }}>
        <footer className="main-footer">
            <div className="row justify-content-between">
                <div className="col">
                    <strong>
                        Copyright © 2021-2022 <a href="https://ironhackvietnam.edu.vn/">Devcamp</a>.
                    </strong>{" "}
                    Author Lam Tan Loc
                </div>
            </div>
        </footer>
    );
};

export default Footer;

import React, { useState, useRef, useEffect } from "react";
import styled from "styled-components";
import useObjectURL from "use-object-url";

const PreviewContainer = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    width: 100%;
    img {
        max-width: 100%;
        height: auto;
        position: relative;
    }
    i {
        position: absolute;
        left: 15px;
        top: 10px;
        cursor: pointer;
    }
`;

const ImageProductUpload = ({ id, resetPreview, imgUrl }) => {
    const [show, setShow] = useState(false);
    const [img, setImg] = useState(null);
    const inputRef = useRef();

    const fileAttachment = (e) => {
        setImg(URL.createObjectURL(e.target.files[0]));
        setShow(true);
    };

    const handleCancelPreview = () => {
        inputRef.current.value = null;
        setImg(null);
        setShow(false);
    };

    useEffect(() => {
        if (imgUrl != null) {
            setShow(true);
        }

        if (resetPreview) {
            inputRef.current.value = null;
            setImg(null);
            setShow(false);
        }
    }, [resetPreview, imgUrl]);

    return (
        <>
            <div className="row">
                <div className="col-12 my-2">
                    {show && imgUrl == null ? (
                        <PreviewContainer>
                            <img id="img__preview" src={img ?? img} alt="product__img" />
                            <i
                                className="fas fa-times-circle fa-2x"
                                onClick={handleCancelPreview}
                            />
                        </PreviewContainer>
                    ) : (
                        ""
                    )}

                    {imgUrl != null && show ? (
                        <PreviewContainer>
                            <img
                                id="img__preview"
                                src={imgUrl.replace(
                                    "C:\\Users\\Admin\\Desktop\\imgStore\\",
                                    "http://127.0.0.1:8080/",
                                )}
                                alt="product__img"
                            />
                            <i
                                className="fas fa-times-circle fa-2x"
                                onClick={handleCancelPreview}
                            />
                        </PreviewContainer>
                    ) : (
                        ""
                    )}
                </div>
                <div className="col my-2">
                    <input
                        id={id}
                        ref={inputRef}
                        type="file"
                        accept="/*"
                        onChange={fileAttachment}
                    />
                </div>
            </div>
        </>
    );
};

export default ImageProductUpload;

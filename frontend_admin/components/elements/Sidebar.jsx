import React from "react";
import { useRouter } from "next/router";
import Link from "next/link";

const Sidebar = () => {
    const router = useRouter();
    return (
        <aside className="main-sidebar sidebar-dark-primary elevation-4">
            {/* Brand Logo */}
            <a href="index3.html" className="brand-link">
                <img
                    src="/dist/img/AdminLTELogo.png"
                    alt="AdminLTE Logo"
                    className="brand-image img-circle elevation-3"
                    style={{ opacity: ".8" }}
                />
                <span className="brand-text font-weight-light">DEVCAMP</span>
            </a>
            {/* Sidebar */}
            <div className="sidebar">
                {/* Sidebar user panel (optional) */}
                <div className="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div className="image">
                        <img
                            src="/dist/img/user2-160x160.jpg"
                            className="img-circle elevation-2"
                            alt="User Image"
                        />
                    </div>
                    <div className="info">
                        <a href="#" className="d-block">
                            Admin
                        </a>
                    </div>
                </div>
                {/* SidebarSearch Form */}
                <div className="form-inline">
                    <div className="input-group" data-widget="sidebar-search">
                        <input
                            className="form-control form-control-sidebar"
                            type="search"
                            placeholder="Search"
                            aria-label="Search"
                        />
                        <div className="input-group-append">
                            <button className="btn btn-sidebar">
                                <i className="fas fa-search fa-fw" />
                            </button>
                        </div>
                    </div>
                </div>
                {/* Sidebar Menu */}
                <nav className="mt-2">
                    <ul
                        className="nav nav-pills nav-sidebar flex-column nav-compact nav-child-indent"
                        data-widget="treeview"
                        role="menu"
                        data-accordion="false"
                    >
                        <li className="nav-item">
                            <Link href={"/"}>
                                <a
                                    className={
                                        router.asPath === "/" ? "nav-link active" : "nav-link"
                                    }
                                >
                                    <i className="nav-icon fas fa-tachometer-alt" />
                                    <p>Home</p>
                                </a>
                            </Link>
                        </li>
                        <li className="nav-item menu-open">
                            <a
                                className={
                                    router.asPath.includes("products")
                                        ? "nav-link active"
                                        : "nav-link"
                                }
                                style={{ cursor: "pointer" }}
                            >
                                <i className="nav-icon fas fa-th" />
                                <p>Products</p>
                                <i className="fas fa-angle-left right" />
                            </a>
                            <ul className="nav nav-treeview">
                                <li className="nav-item">
                                    <Link href="/products">
                                        <a
                                            className={
                                                router.asPath === "/products"
                                                    ? "nav-link active"
                                                    : "nav-link"
                                            }
                                        >
                                            <i className="far fa-circle nav-icon" />
                                            <p>All products</p>
                                        </a>
                                    </Link>
                                </li>
                                <li className="nav-item">
                                    <Link href="/products/create">
                                        <a
                                            className={
                                                router.asPath.includes("create")
                                                    ? "nav-link active"
                                                    : "nav-link"
                                            }
                                        >
                                            <i className="far fa-circle nav-icon" />
                                            <p>Create new</p>
                                        </a>
                                    </Link>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
            {/* /.sidebar */}
        </aside>
    );
};

export default Sidebar;

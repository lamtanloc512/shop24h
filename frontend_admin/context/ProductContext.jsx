import React, { createContext, useReducer } from "react";

import { ProductReducer, ProductAction } from "./../reducer/ProductReducer";

const ProductInitial = {
    products: null,
    payload: {},
    status: {
        loading: false,
        success: null,
    },
};

export const ProductContext = createContext();

export const ProductContextProvider = ({ children }) => {
    const [productState, dispatch] = useReducer(ProductReducer, ProductInitial);

    const getAllProducts = () => {
        let myHeaders = new Headers();
        myHeaders.append("Authorization", "Bearer ");

        let requestOptions = {
            method: "GET",
            headers: myHeaders,
            redirect: "follow",
        };
        let result = [];

        fetch("http://localhost:9000/product/all", requestOptions)
            .then((response) => (result = response.json()))
            .then((result) => {
                dispatch({ type: ProductAction.GET_PRODUCTS, payload: result });
            })
            .catch((error) => console.log("at Product context 1", error));
    };

    const createNewProduct = (payload) => {
        let formData = new FormData();

        if (!payload.img) {
            alert("You need upload image");
        } else {
            formData.append("product", `${JSON.stringify(payload.product)}`);
            formData.append("img", payload.img);
            fetch("http://localhost:9000/product/create", {
                method: "POST",
                body: formData,
                redirect: "follow",
            })
                .then((response) => response.json())
                .then((result) => {
                    dispatch({ type: ProductAction.CREATE_PRODUCT_SUCCESS, payload: result });
                })
                .catch((error) => {
                    console.log("error", error);
                    dispatch({ type: ProductAction.CREATE_PRODUCT_ERROR, payload: error });
                    dispatch({ type: ProductAction.RESET_LOADING });
                });
        }
    };
    const resetLoading = () => {
        dispatch({ type: ProductAction.RESET_LOADING });
    };

    const deleteProduct = (id) => {
        // call api delete product
        fetch(`http://localhost:9000/product/delete/${id}`, {
            method: "DELETE",
        })
            .then((response) => response.text())
            .then((result) => {
                dispatch({ type: ProductAction.DELETE_PRODUCT_SUCCESS, payload: result });
                dispatch({ type: ProductAction.RESET_LOADING });
            })
            .catch((error) => {
                console.log("error", error);
                dispatch({ type: ProductAction.DELETE_PRODUCT_ERROR, payload: error });
            });
    };

    return (
        <ProductContext.Provider
            value={{
                productState: productState.payload,
                status: productState.status,
                products: productState.products,
                getAllProducts,
                createNewProduct,
                resetLoading,
                deleteProduct,
            }}
        >
            {children}
        </ProductContext.Provider>
    );
};

export default ProductContextProvider;
